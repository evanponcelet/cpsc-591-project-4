/* 
  Alexandra Gaspar, Evan Poncelet
  October 25, 2013

  Description: This header file defines the global variables needed and all the
               structs to be used in the rest of the program
*/

#ifndef STRUCTS_H_
#define STRUCTS_H_

//simulate boolean type
enum myBool { FALSE = 0, TRUE = 1 };
typedef enum myBool Bool;

struct departingTrain
{
  short departureDirection;
  unsigned short trainSize;
};
typedef struct departingTrain departingTrain;

//Global variables

//new Adds
extern unsigned short arrivingTrainDistance[8]; //
extern departingTrain departingQueue[8]; //for now just an array designer's choice type
extern unsigned short arrivingWhistle; //
extern unsigned short departingWhistle; //
extern unsigned short departingFlag; //
extern unsigned int temperatureBuf[8]; //
extern Bool wheelTemp; //
extern unsigned int numDepartingTrains;
extern signed int timeData[256];
extern unsigned int freqData[16];
extern Bool freqDataReady;
//end new Adds

//new mods
extern unsigned short trainPresent; //
//end new mods

extern Bool flashOn;
extern unsigned int continueFlashing;
extern long setBits;

extern Bool checkBits;

extern unsigned short arrivalDirection; //
extern unsigned short departureDirection; // designer's choice type
extern unsigned short trainSize; //
extern unsigned short traversalTime; //
extern unsigned short trainArriving;  //
extern unsigned short mode; //
extern unsigned short statusSelection; //
extern unsigned short scroll; // 
extern short select; //
extern unsigned short annunciation; //
extern Bool checkTrain; //

extern Bool gridlock; //
extern unsigned int counter;
extern unsigned int freqCounter;
extern Bool freqDataReady;
extern Bool timeDataReady;

//Additional Globals
extern unsigned short fiveSecondCount;
extern unsigned short fourSecondCount;
extern unsigned short threeSecondCount;
extern unsigned short twoSecondCount;
extern unsigned short oneSecondCount;

extern unsigned int arrivalTime;
extern Bool annuncArrival;
extern Bool annuncDeparture;

extern unsigned const int flashingRateNorthTrain;
extern unsigned const int flashingRateEastTrain;
extern unsigned const int flashingRateWestTrain;
extern unsigned const int flashingRateSouthTrain;

extern Bool trainInfoDisplay;
extern Bool specificTrainDisplay;

//parameters for Arrival blasts
extern int aBeepStartCount;
extern Bool aBeepLongMode;
extern Bool aBeepSilenceMode;
extern int aBeepLONG_BLAST;
extern int aBeepSHORT_BLAST;
extern int aBeepSILENCE;
extern int aBeepTOTAL_LONG_BLASTS;
extern int aBeepTOTAL_SHORT_BLASTS;
extern int aBeepNumLongBlasts;
extern int aBeepNumShortBlasts;

//parameters for Departure blasts
extern int dBeepStartCount;
extern Bool dBeepLongMode;
extern Bool dBeepSilenceMode;
extern int dBeepLONG_BLAST;
extern int dBeepSHORT_BLAST;
extern int dBeepSILENCE;
extern int dBeepTOTAL_LONG_BLASTS;
extern int dBeepTOTAL_SHORT_BLASTS;
extern int dBeepNumLongBlasts;
extern int dBeepNumShortBlasts;

extern int rapidTrainDistance[8];

//Arriving train data structs
struct ArrivingTrainStruct
{
  unsigned short* trainArriving;
  Bool* checkTrain;
  unsigned short* arrivalDirection;
  unsigned short* arrivingTrainDistance;//
};
typedef struct ArrivingTrainStruct ArrivingTrainStruct;


//Departing train data structs
struct DepartingTrainStruct
{
  unsigned short* trainPresent;// 
  unsigned short* departingFlag;//
  unsigned short* departureDirection;
  departingTrain* departingQueue;// 
};
typedef struct DepartingTrainStruct DepartingTrainStruct;

//Temperature measurement data structs
struct TempMeasStruct
{
  unsigned short* trainPresent; 
  unsigned int* temperatureBuf;
};
typedef struct TempMeasStruct TempMeasStruct;

//Train communication data struct
struct TrainComStruct
{
  unsigned short* trainPresent;
  Bool* checkTrain;
  unsigned short* departingFlag;
  unsigned short* trainSize;
  unsigned short* trainArriving;
  unsigned short* arrivalDirection;
  unsigned short* departureDirection;
  departingTrain* departingQueue;
};
typedef struct TrainComStruct TrainComStruct;

//OLED display data struct
struct DisplayStruct
{
  unsigned short* trainPresent;
  unsigned short* trainArriving;
  unsigned short* trainSize; 
  unsigned short* traversalTime;
  unsigned short* arrivalDirection;
  unsigned short* departureDirection;
  unsigned int* temperatureBuf;
  unsigned short* arrivingTrainDistance;
  unsigned short* departingFlag;  
  unsigned short* mode;
  unsigned short* statusSelection;
  unsigned short* scroll;
  short* select;
  unsigned short* annunciation;
};

typedef struct DisplayStruct DisplayStruct;

//Switch control data struct
struct SwitchControlStruct
{
  departingTrain* departingQueue;
  unsigned short* departingFlag;
  unsigned short* trainPresent;
  Bool* gridlock;
  unsigned short* trainSize; 
  unsigned short* traversalTime;
};
typedef struct SwitchControlStruct SwitchControlStruct;

//local keypad struct
struct LocalKeypadStruct
{
  unsigned short* mode;
  unsigned short* statusSelection;
  unsigned short* scroll;
  short* select;
  unsigned short* annunciation; 
};
typedef struct LocalKeypadStruct LocalKeypadStruct;

//serial communications data
struct SerialComStruct
{
  Bool* gridlock;
  unsigned short* trainPresent;
  unsigned short* trainArriving;
  unsigned short* trainSize;
  unsigned short* traversalTime;
  unsigned short* arrivalDirection;
  unsigned short* departureDirection;
  unsigned int* temperatureBuf;
  //added extra global for when train distance percentage difference exceeds
  //10%
  unsigned short* rapidTrainDistance;
};
typedef struct SerialComStruct SerialComStruct;

struct noiseSignatureCaptureStruct
{
  signed int* timeData;
  Bool* timeDataReady;
  
};
typedef struct noiseSignatureCaptureStruct noiseCaptureStruct;

struct noiseSignatureProcessStruct
{
  Bool* timeDataReady;
  signed int* timeData;
  unsigned int* freqData;
  unsigned int* freqDataReady;
};
typedef struct noiseSignatureProcessStruct noiseProcessStruct;
#endif