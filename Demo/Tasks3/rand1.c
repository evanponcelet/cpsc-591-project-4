/*
  Alexandra Gaspar, Evan Poncelet
  October 31, 2013

  Description: Random number generator

  Citation for this code since it was developped by others
  - Author: James Peckol
  - Date: October 3, 2013
  - Title of program/source code: rand1.c
  - Code version: 1
  - Type: source code
  - Web address: http://oxconltd.com/SeattleU/code/code.html
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <limits.h>

int seed = 1;

int randomInteger(int low, int high)
{
  double randNum = 0.0;
  int multiplier = 2743;
  int addOn = 5923;
  double max = INT_MAX + 1.0;

  if (low > high)
    return randomInteger(high, low);
  else
  {
    seed = seed*multiplier + addOn;
    randNum = seed;

    if (randNum <0)
      randNum = randNum + max;

    randNum = randNum/max;

    return ((int)((high-low+1)*randNum))+low;
   }
}


