/*
  Alexandra Gaspar, Evan Poncelet
  December 3, 2013

  Description: FFT function

  Citation for this code since it was developped by others
  - Author: Brent Plump
  - Date: December 3, 2013
  - Title of program/source code: optfft.h
  - Type: source code
  - Web address: http://oxconltd.com/SeattleU/code/code.html
*/
signed int optfft(signed int x[256], signed int y[256]);