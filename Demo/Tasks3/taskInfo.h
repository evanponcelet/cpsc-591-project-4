/* 
  Alexandra Gaspar, Evan Poncelet
  October 25, 2013

  Description: This header file defines function prototypes for the tasks
*/

#ifndef TASK_INFO_H_
#define TASK_INFO_H_

//train functions
void arrivingTrainFcn(void*);
void departingTrainFcn(void*);

//temperature measurement function
void tempMeasFcn(void*);

//train communication functions
void trainComFcn(void*);

//oled display functions
void oledDisplayFcn(void*);

//switch control functions
void switchControlFcn(void*);

//local keypad function
void localKeypadFcn(void*);

//serial communcations function
void serialComFcn(void*);

#endif