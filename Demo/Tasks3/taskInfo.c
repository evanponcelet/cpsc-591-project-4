/*
  Alexandra Gaspar, Evan Poncelet
  October 25, 2013

  Description: Implements the functions of the tasks
*/

#include "structs.h"
#include "string.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "taskInfo.h"
#include <math.h>
#include <limits.h>
#include "C:/StellarisWare/boards/ek-lm3s8962/drivers/rit128x96x4.h"
#include "C:/StellarisWare/inc/lm3s8962.h"
//includes needed for beeping
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"
//includes for serial communication 
#include "driverlib/uart.h"

// Send a string to the UART
//*****************************************************************************
void UARTSend(const unsigned char *pucBuffer, unsigned long ulCount)
{
    //
    // Loop while there are more characters to send.
    //
    while(ulCount--)
    {
        //
        // Write the next character to the UART.
        //
        UARTCharPut(UART0_BASE, *pucBuffer++);
    }
}

// Purpose: determines if there is a train present, determines its size and
//          desired outbound direction
// Params: dataStruct -> holds  a trainCom data struct used to access global
//                       information
void trainComFcn(void* dataStruct)
{
    //UARTSend((unsigned char *)"\r\nTC ", 4);
    TrainComStruct* TrainComDataConverter = dataStruct;
    TrainComStruct trainComData = *TrainComDataConverter;
    
    static Bool firstStart = TRUE;
    static int fiveSecCounter = 50;
    Bool sameFlag = FALSE;
    
    if((fiveSecCounter < 1) || firstStart)
    {  
        //prepare to wait five seconds before executing again
        firstStart = FALSE;
        fiveSecCounter = fiveSecondCount;
        
        //generate departure direction
        departureDirection = randomInteger(0,4);
        
        //generate trainsize
        trainSize = randomInteger(1,9);
        
        //set train present
        trainPresent = TRUE;
        
        //decrement trainArriving variable
        if(trainArriving > 0)
          trainArriving --;
        
        checkTrain = FALSE;
    }
    
    fiveSecCounter --;
    
    
     
}

// Purpose: Determines how an arriving train interacts with the train management system
// Params: dataStruct -> holds  a north arrivingTrainData struct used to access global
//                       information
void arrivingTrainFcn(void* dataStruct)
{   
    //UARTSend((unsigned char *)"\r\nAT ", 4);
    ArrivingTrainStruct* arrivingTrainDataConverter = dataStruct;
    ArrivingTrainStruct arrivingTrainData = *arrivingTrainDataConverter;
    
    static Bool newArrival = TRUE;
    static Bool firstStart = TRUE;
    static Bool beep = FALSE;
    static unsigned int startCount = 0;
    static unsigned int startDelayCount = 0; 
    
    static int fiveSecCounter = 0;
    
      //if 5 second delay is up or this is the first run,
      //not awaiting another train's arrival, and if there is a train in the arrival queue
      //calculate direction and arrivalTime
      //if(((((counter - StartCount)> fiveSecondCount) && !noMajorJob)|| firstStart) && (trainArriving > 0))
      if(((fiveSecCounter >= fiveSecondCount) && (trainArriving > 0)) && (!beep))
      {
          fiveSecCounter = 0;
          arrivalDirection = randomInteger(1,4); //1-North, 2-south, 3-east, 4-west
          arrivalTime = randomInteger(1,9);
          
          //checking randomness
          //char myc[30];
          //sprintf(myc,(char*)"%d",arrivalTime);
          
          arrivalTime = arrivalTime * 240; //.4 * 1minute(600tenths of a second) = 240 tenths of a sec
          startDelayCount = arrivalTime; //starting point for a new delay
          beep = TRUE;
          
          //checking randomness
          //char nyc[30];
          //sprintf(myc,(char*)"%d",arrivalTime);
      }
      fiveSecCounter++;
      
      if((startDelayCount <= 0) && (arrivalTime > 0))
      {
         beep = FALSE;
  
         checkTrain = TRUE; //signal the scheduler to add the trainCom task
      } 
      startDelayCount --;
  
      //if it's not a new train or it's direction is OFF, don't bother beeping
      if(beep && (arrivalDirection !=0))
      {
        aBeepStartCount = 0;
        switch(arrivalDirection)
        {
          case(1):  //north            
                  aBeepLongMode = TRUE;
                  aBeepSilenceMode = FALSE;
                  aBeepLONG_BLAST = twoSecondCount;
                  aBeepSHORT_BLAST = oneSecondCount;
                  aBeepSILENCE = oneSecondCount;
                  aBeepTOTAL_LONG_BLASTS = 2;
                  aBeepTOTAL_SHORT_BLASTS = 2;
                  aBeepNumLongBlasts = 2;
                  aBeepNumShortBlasts = 2;
                  beep = TRUE;
                  break;
         case(2):  //South
                  aBeepLongMode = TRUE;
                  aBeepSilenceMode = FALSE;
                  aBeepLONG_BLAST = twoSecondCount;
                  aBeepSHORT_BLAST = threeSecondCount;
                  aBeepSILENCE = oneSecondCount;
                  aBeepTOTAL_LONG_BLASTS = 2;
                  aBeepTOTAL_SHORT_BLASTS = 2;
                  aBeepNumLongBlasts = 2;
                  aBeepNumShortBlasts = 2;
                  beep = TRUE;
                  break;
         case(3): //East
                  aBeepLongMode = TRUE;
                  aBeepSilenceMode = FALSE;
                  aBeepLONG_BLAST = twoSecondCount;
                  aBeepSHORT_BLAST = oneSecondCount;
                  aBeepSILENCE = oneSecondCount;
                  aBeepTOTAL_LONG_BLASTS = 3;
                  aBeepTOTAL_SHORT_BLASTS = 2;
                  aBeepNumLongBlasts = 3;
                  aBeepNumShortBlasts = 2;
                  beep = TRUE;
                  break;
         case(4): //West
                  aBeepLongMode = TRUE;
                  aBeepSilenceMode = FALSE;
                  aBeepLONG_BLAST = twoSecondCount;
                  aBeepSHORT_BLAST = oneSecondCount;
                  aBeepSILENCE = oneSecondCount;
                  aBeepTOTAL_LONG_BLASTS = 1;
                  aBeepTOTAL_SHORT_BLASTS = 2;
                  aBeepNumLongBlasts = 1;
                  aBeepNumShortBlasts = 2;
                  beep = TRUE;
                  break;
        default:
                  beep = FALSE;
                  break;
        }
      }
      if(beep && ((aBeepNumShortBlasts > 0) || (aBeepNumLongBlasts > 0)))
      { 
        //if new seqeunce is starting then reset to start with longBlasts
        //regardless of when interrupted (may be useful in future)
        /*if(arrivingTime >= 0)
        {
           aBeepLongMode = TRUE; 
           aBeepNumLongBlasts = aBeepTOTAL_LONG_BLASTS;
           aBeepNumShortBlasts = aBeepTOTAL_SHORT_BLASTS;
        } */ 
       
        //if new beeping interval, then turn on beeper and save the time as aBeepStartCount
        if((aBeepStartCount == 0) && !aBeepSilenceMode)
        {
          PWMGenEnable(PWM_BASE, PWM_GEN_0);
          aBeepStartCount = counter;
        }
        
        //in the future possible case that we get a train with only long or only
        //short blasts
        /*
        if(aBeepTOTAL_LONG_BLASTS == 0)
          aBeepLongMode = FALSE;
        if(aBeepTOTAL_SHORT_BLASTS ==0)
          aBeepLongMode = TRUE;*/
        
        //if in longblast mode, turn off beep after a long blast duration
        //prepare new interval of silence, and if all done with long blasts, set 
        //mode to shortblast mode
        if((((counter - aBeepStartCount) > aBeepLONG_BLAST) && aBeepLongMode) && !aBeepSilenceMode)
        {
          PWMGenDisable(PWM_BASE, PWM_GEN_0);
          aBeepSilenceMode = TRUE;
          aBeepStartCount = 0;
          aBeepNumLongBlasts --; //decrement the number of longblasts left to be performed
          if((aBeepNumLongBlasts == 0))  //if no more longblasts left re-inint aBeepNumLongBlasts
                                    //and put in shortMode
          {
             //aBeepNumLongBlasts = aBeepTOTAL_LONG_BLASTS;
             aBeepLongMode = FALSE;
          }
        }
        //if shortblast mode, then turn off beep after a short blast duration
        //prepare new interval of silence, if all done with short blasts, set mode 
        //to longblast mode
        else if((((counter - aBeepStartCount) > aBeepSHORT_BLAST) && !aBeepLongMode) && !aBeepSilenceMode)
        {
          PWMGenDisable(PWM_BASE, PWM_GEN_0);
          aBeepSilenceMode = TRUE;
          aBeepStartCount = 0;
          aBeepNumShortBlasts --; //decrement the number of shortblasts left to be performed
          if((aBeepNumShortBlasts == 0)) //if no more shortblasts left re-inint 
                                    //aBeepNumShortBlasts and put in shortMode
          {
             //aBeepNumShortBlasts = aBeepTOTAL_SHORT_BLASTS;
             aBeepLongMode = TRUE;
          }
        }
        
        //if new silence interval, then save the time as aBeepStartCount; 
        if((aBeepStartCount ==0) && aBeepSilenceMode)
        {
            aBeepStartCount = counter;
        }
        //check to see if (newcount - aBeepStartCount) > silence interval duration
        //if so, reset aBeepStartCount and turn off aBeepSilenceMode so that either long or 
        //short blasting can occur
        if(aBeepSilenceMode && ((counter - aBeepStartCount) > aBeepSILENCE))
        {
            aBeepStartCount = 0;
            aBeepSilenceMode = FALSE;
        }
        
      }
    
}


// Purpose: Determines how an arriving train interacts with the train management system
// Params: dataStruct -> holds  a north departingTrainData struct used to access global
//                       information
void departingTrainFcn(void* dataStruct)
{
  /*
    int tTraversalTime = 300;
    tTraversalTime--;
    //UARTSend((unsigned char *)"\r\nDT ", 4);
    DepartingTrainStruct* departingTrainDataConverter = dataStruct;
    DepartingTrainStruct departingTrainData = *departingTrainDataConverter;
    
    departureDirection = 1;
    
    static Bool newDeparture = TRUE;
    static Bool firstStart = TRUE;
    static Bool beep = FALSE;
    static unsigned int StartCount = 0;
    static unsigned int startDelayCount = 0; 
    static Bool noMajorJob = FALSE; //is set to false while a train is arriving
    //check to see if 5 second delay is up
    if((((counter - StartCount)> fiveSecondCount)|| firstStart) && !noMajorJob)
    {
         StartCount = counter;
         firstStart = FALSE;
         noMajorJob = TRUE;
         if (departureDirection !=0)
            startDelayCount = counter; //starting point for a new delay
    }
    
    if(((counter - startDelayCount) > tTraversalTime))
    {
       newDeparture = TRUE; //once the delay for an traversing rain is past, it's safe
                          //to say that any train that is present, is ready to leave
       noMajorJob = FALSE; //allow for a new departure to begin
    }
    

    //if it's not a new train or it's direction is OFF, don't bother beeping
    if(newDeparture && (departureDirection !=0))
    {
      dBeepStartCount = 0;
      newDeparture = FALSE; //only set the blasts once for each arriving train
      switch(departureDirection)
      {
        case(1):  //north            
                dBeepLongMode = TRUE;
                dBeepSilenceMode = FALSE;
                dBeepLONG_BLAST = twoSecondCount;
                dBeepSHORT_BLAST = oneSecondCount;
                dBeepSILENCE = oneSecondCount;
                dBeepTOTAL_LONG_BLASTS = 1;
                dBeepTOTAL_SHORT_BLASTS = 2;
                dBeepNumLongBlasts = 1;
                dBeepNumShortBlasts = 2;
                beep = TRUE;
                break;
       case(2):  //South
                dBeepLongMode = TRUE;
                dBeepSilenceMode = FALSE;
                dBeepLONG_BLAST = threeSecondCount;
                dBeepSHORT_BLAST = oneSecondCount;
                dBeepSILENCE = oneSecondCount;
                dBeepTOTAL_LONG_BLASTS = 1;
                dBeepTOTAL_SHORT_BLASTS = 2;
                dBeepNumLongBlasts = 2;
                dBeepNumShortBlasts = 2;
                beep = TRUE;
                break;
       case(3): //East
                dBeepLongMode = TRUE;
                dBeepSilenceMode = FALSE;
                dBeepLONG_BLAST = fourSecondCount;
                dBeepSHORT_BLAST = oneSecondCount;
                dBeepSILENCE = oneSecondCount;
                dBeepTOTAL_LONG_BLASTS = 3;
                dBeepTOTAL_SHORT_BLASTS = 2;
                dBeepNumLongBlasts = 3;
                dBeepNumShortBlasts = 2;
                beep = TRUE;
                break;
       case(4): //West
                dBeepLongMode = TRUE;
                dBeepSilenceMode = FALSE;
                dBeepLONG_BLAST = fiveSecondCount;
                dBeepSHORT_BLAST = oneSecondCount;
                dBeepSILENCE = oneSecondCount;
                dBeepTOTAL_LONG_BLASTS = 1;
                dBeepTOTAL_SHORT_BLASTS = 2;
                dBeepNumLongBlasts = 1;
                dBeepNumShortBlasts = 2;
                beep = TRUE;
                break;
      default:
                beep = FALSE;
                break;
      }
    }
    if(beep)
    { 
      //if new beeping interval, then turn on beeper and save the time as dBeepStartCount
      if((dBeepStartCount == 0) && !dBeepSilenceMode)
      {
        PWMGenEnable(PWM_BASE, PWM_GEN_0);
        dBeepStartCount = counter;
      }
      
      //if in longblast mode, turn off beep after a long blast duration
      //prepare new interval of silence, and if all done with long blasts, set 
      //mode to shortblast mode
      if((((counter - dBeepStartCount) > dBeepLONG_BLAST) && dBeepLongMode) && !dBeepSilenceMode)
      {
        PWMGenDisable(PWM_BASE, PWM_GEN_0);
        dBeepSilenceMode = TRUE;
        dBeepStartCount = 0;
        dBeepNumLongBlasts --; //decrement the number of longblasts left to be performed
        if((dBeepNumLongBlasts == 0))  //if no more longblasts left re-inint dBeepNumLongBlasts
                                  //and put in shortMode
        {
           dBeepNumLongBlasts = dBeepTOTAL_LONG_BLASTS;
           dBeepLongMode = FALSE;
        }
      }
      //if shortblast mode, then turn off beep after a short blast duration
      //prepare new interval of silence, if all done with short blasts, set mode 
      //to longblast mode
      else if((((counter - dBeepStartCount) > dBeepSHORT_BLAST) && !dBeepLongMode) && !dBeepSilenceMode)
      {
        PWMGenDisable(PWM_BASE, PWM_GEN_0);
        dBeepSilenceMode = TRUE;
        dBeepStartCount = 0;
        dBeepNumShortBlasts --; //decrement the number of shortblasts left to be performed
        if((dBeepNumShortBlasts == 0)) //if no more shortblasts left re-inint 
                                  //dBeepNumShortBlasts and put in shortMode
        {
           dBeepNumShortBlasts = dBeepTOTAL_SHORT_BLASTS;
           dBeepLongMode = TRUE;
        }
      }
      
      //if new silence interval, then save the time as dBeepStartCount; 
      if((dBeepStartCount ==0) && dBeepSilenceMode)
      {
          dBeepStartCount = counter;
      }
      //check to see if (newcount - dBeepStartCount) > silence interval duration
      //if so, reset dBeepStartCount and turn off dBeepSilenceMode so that either long or 
      //short blasting can occur
      if(dBeepSilenceMode && ((counter - dBeepStartCount) > dBeepSILENCE))
      {
          dBeepStartCount = 0;
          dBeepSilenceMode = FALSE;
      }
      
    } */
}

// Purpose: sets and cleares data on the OLED display
// Params: dataStruct -> holds  a displayData struct used to access global
//                       information
void oledDisplayFcn(void* dataStruct)
{
/*  
  //UARTSend((unsigned char *)"\r\nOD ", 4);
    DisplayStruct* displayDataConverter = dataStruct;
    DisplayStruct displayData = *displayDataConverter;
   
        
    if (gridlockDelay != 0)             //there is a gridlock
    {
      if (trainInfoDisplay == TRUE)    
      {
        //train information is currently displayed so the display must first be
        //cleared
        // Initialize the OLED display.
        RIT128x96x4Init(1000000);
        
        trainInfoDisplay = FALSE;
      }
  
      RIT128x96x4StringDraw("WARNING: Gridlock ", 10, 35, 15);
    }
    else if (fiveSecDelay == 0)
    {
      if (*displayData.trainPresent == TRUE) 
      {
        if (trainInfoDisplay == FALSE)
        {
          //a warning is currently displayed so the display must first be cleared
          // Initialize the OLED display.
          RIT128x96x4Init(1000000);
        
          trainInfoDisplay = TRUE;
        }
        //task complete so must wait for 5 seconds before beginning again
        fiveSecDelay = 50;
      }
    }
    else
    {
      //decrement time left of delay
      fiveSecDelay--;
    }
    
    //minor jobs
    //if being delayed and there is no gridlock
    if (gridlock == 0)
    {
      //traversal time can't be 0. If it is, switchControl didn't have a chance to
      //run so all must wait for switchControl to do its job
      if (*displayData.traversalTime != 0)
      {
        if (*displayData.north == '1')
        {
          flash(&flashingRateNorthTrain, dataStruct); 
        }
        else if (*displayData.east == '1')
        {
          flash(&flashingRateEastTrain, dataStruct); 
        }
        if (*displayData.west == '1')
        {
          flash(&flashingRateWestTrain, dataStruct); 
        }
      }
    }
  }
*/
}

// Purpose: sets and handles gridlocks, and instructs trains when to traverse
// Params: dataStruct -> holds a switchData struct used to access global
//                       information
void switchControlFcn(void* dataStruct)
{
    //UARTSend((unsigned char *)"\r\nSC ", 4);
    SwitchControlStruct* switchControlDataConverter = dataStruct;
    SwitchControlStruct switchControldData = *switchControlDataConverter;
    
    static unsigned int fiveSecCounter =  50; 
    static Bool firstPass = TRUE;
    static unsigned int gridlockDelay = 0;
    
    if(((fiveSecCounter < 1)|| firstPass) && (traversalTime < 1))
    {
        fiveSecCounter = fiveSecondCount;
        
        if(trainPresent != 0)
        {
            unsigned int n = randomInteger(-9,9);
              
            if(n < 0)
            {
                gridlockDelay = ((n*n)/n) * 120; //.2 *600 tenths of a second;
                gridlock = TRUE;
            }
            
            if( !nLock && !eLock && !sLock && !wLock)
            {
                switch(departureDirection)
                {
                case 1: sLock =TRUE; eLock=TRUE; wLock=TRUE; nLock=TRUE; break;
                case 2: nLock = TRUE; eLock = TRUE; wLock = TRUE; sLock = TRUE; break;
                case 3: nLock = TRUE; sLock = TRUE; wLock = TRUE; eLock = TRUE; break;
                case 4: eLock = TRUE; sLock = TRUE; nLock = TRUE; wLock = TRUE; break;
                default: break;
                }
                traversalTime = trainSize * 180; //.3 * 600 tenths of a second 
                annuncDeparture = TRUE;
            }
           
        }
           
    }
    
    
    if((traversalTime < 1) && trainPresent)
    {
       trainPresent = FALSE;
       trainSize = 0;
       
       switch(departureDirection)
       {
       case 1: nLock=FALSE;sLock =FALSE; eLock=FALSE; wLock=FALSE;  break;
       case 2: sLock = FALSE; nLock = FALSE; eLock = FALSE; wLock = FALSE; break;
       case 3: eLock = FALSE; nLock = FALSE; sLock = FALSE; wLock = FALSE; break;
       case 4: wLock = FALSE; eLock = FALSE; sLock = FALSE; nLock = FALSE; break;
       default: break;
      }
       departureDirection = 0;
    }
    
    traversalTime --;
    
}


// Purpose: assists with identification and selection of user information to be displayed
//          and acknowledges alarms ignals.
// Params: dataStruct -> holds a localKeypad struct used to access global
//                       information
void localKeypadFcn(void* dataStruct)
{
    //UARTSend((unsigned char *)"\r\nLK ", 4);
    LocalKeypadStruct* localKeypadDataConverter = dataStruct;
    LocalKeypadStruct localKeypadData = *localKeypadDataConverter;
    
    /* the interrupts work, but alas this section was not able to be completed 
       due to the learning curve of the timers/gpio/ c memory managment etc*/
}


// Purpose: displays status information and warnings in hyperterminal
// Params: dataStruct -> holds a SerialCom struct used to access global
//                       information
void serialComFcn(void* dataStruct)
{   
    //UARTSend((unsigned char *)"\r\nSeC ", 5);
    SerialComStruct* serialComDataConverter = dataStruct;
    SerialComStruct serialComData = *serialComDataConverter;
    
    static unsigned int twoSecCounter = 20;
    static Bool firstPass = TRUE;
  
    if((twoSecCounter < 1 ) || firstPass)
    {
      twoSecCounter = 5;
      firstPass = false;
      char *buffer;
      int memsize = 300;
      buffer = malloc (sizeof(char) * memsize);  //must use malloc, can't statically allocate > 120 chars or so
      if(buffer == 0) //check to see if memory was even allocated (isr should be called if this happens)
      {
        char mem[]="Out of Memory";  
        UARTSend((unsigned char*)mem,13);
        return; //if there's not enough memory to display anything, just return. 
      }  
      memset(buffer,0,300);
      
      char dDirection[5] = "";
      char Direction[5] = "";
      char intAvailable[3] = "";
      char glOn[3] = "";
      int n;
      char* point = NULL;
      char nor[] = "North";
      char e[] = "East";
      char s[] = "South";
      char w[] = "West";
      char o[] = "Off";
      char yes[] = "Yes";
      char no[] = "No";
      switch(*serialComData.departureDirection)
      {
        case 0: 
          n = sprintf(dDirection,"%s",o);
        break;
        case 1: 
        n = sprintf(dDirection, "%s",nor);
        break;
        case 2:
        n = sprintf(dDirection,"%s",s);
        break;
        case 3: 
        n = sprintf(dDirection,"%s",e);
        break;
        case 4: 
        n = sprintf(dDirection,"%s",w);
        break;
        default: 
        n = sprintf(dDirection,"%s",o);
      }
     
       switch(*serialComData.departureDirection)
      {
        case 0: 
          n = sprintf(Direction,"%s",o);
        break;
        case 1: 
        n = sprintf(Direction, "%s",nor);
        break;
        case 2:
        n = sprintf(Direction,"%s",s);
        break;
        case 3: 
        n = sprintf(Direction,"%s",e);
        break;
        case 4: 
        n = sprintf(Direction,"%s",w);
        break;
        default: 
        n = sprintf(Direction,"%s",o);
      }
      
      if(*serialComData.traversalTime < 1)
         n = sprintf(intAvailable,"%s",yes);
      else 
         n = sprintf(intAvailable,"%s",no);
      
      if(*serialComData.gridlock == TRUE)
         n = sprintf(glOn,"%s", yes);
      else 
         n = sprintf(glOn,"%s",no);
      
      int l = sprintf(buffer, "\fTrain Present              %d\r\nCurrent Direction          %s\r\nTrain Size                 %d\r\nTraversal Time             %d\r\nIntersection Availability  %s\r\nTrain Arriving             %s\r\nArrival Time               %d\r\nGridlock                   %s", trainArriving, dDirection,trainSize,traversalTime,intAvailable,Direction,arrivalTime,glOn);
      UARTSend((unsigned char*)buffer,memsize);
      free(buffer); //return the memory used to store status character strings
   }
   twoSecCounter --;
}

//temperature measurement function
void tempMeasFcn(void*)
{
}