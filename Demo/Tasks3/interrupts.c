/*
  Alexandra Gaspar, Evan Poncelet
  October 31, 2013

  Description: Implements the interrupt service routines (ISRs)
*/


#include <stdio.h>
#include "C:/StellarisWare/boards/ek-lm3s8962/drivers/rit128x96x4.h"
//includes for shared hardware management
#include "structs.h"
#include "inc/lm3s8962.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
//includes needed for beeping
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"
//includes for interrupts
#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"
//includes for hardware timer
#include "driverlib/timer.h"
//includes for serial communication 
#include "driverlib/uart.h"


// The UART interrupt handler.
//****************************************************************************

void UARTIntHandler(void)
{
    unsigned long ulStatus;

    //
    // Get the interrrupt status.
    //
    ulStatus = UARTIntStatus(UART0_BASE, true);

    //
    // Clear the asserted interrupts.
    //
    UARTIntClear(UART0_BASE, ulStatus);

    //
    // Loop while there are characters in the receive FIFO.
    //
    while(UARTCharsAvail(UART0_BASE))
    {
        //
        // Read the next character from the UART and write it back to the UART.
        // The nonblocking one in the sample code quits when the UART tx fifo is full,
        // failing to output more characters at that point. 
        UARTCharPut(UART0_BASE, UARTCharGetNonBlocking(UART0_BASE));
    }
}


// The interrupt handler for the hardware timer
//*****************************************************************************
volatile void FFTSampleTimer(void)
{
    //
    // Clear the timer interrupt.
    //
    TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
    freqCounter++;

}

// The interrupt handler for the hardware timer
//*****************************************************************************
volatile void timer1BIntHandler(void)
{
    //
    // Clear the timer interrupt.
    //
    TimerIntClear(TIMER1_BASE, TIMER_TIMB_TIMEOUT);
    freqCounter++;
}

// The interrupt handler for the sw2 being pressed
//*****************************************************************************
volatile void trainArrived(void)
{   
    
    // clear the interrupt
    GPIOPinIntClear(GPIO_PORTF_BASE, GPIO_PIN_1);
    trainArriving++;
       
}

// The interrupt handler for a key on the keypad being pressed
//*****************************************************************************
volatile void keyPress(void)
{   
    // clear the interrupt
    setBits = GPIOPinIntStatus(GPIO_PORTE_BASE,true);
    GPIOPinIntClear(GPIO_PORTE_BASE, GPIO_PIN_0);
    GPIOPinIntClear(GPIO_PORTE_BASE, GPIO_PIN_1);
    GPIOPinIntClear(GPIO_PORTE_BASE, GPIO_PIN_2);
    GPIOPinIntClear(GPIO_PORTE_BASE, GPIO_PIN_3);
    
    if (setBits != 0)
      checkBits = TRUE;
    /*
    if(setBits % 2 == 1) //if leftmost bit (bit0) is 1 then UP button (sw3) pressed
    {
      GPIOPinIntClear(GPIO_PORTE_BASE, GPIO_PIN_0);
      //make sure scroll does not exceed max trainsList size and that up scrolling only happens in status mode
      if((scroll < trainArriving) && (mode == 1))
          scroll ++;
          //trainArriving--; //TODO: remove debug
          //list all trains at or above scroll
    }
    
    setBits = setBits >> 1; //shift out previous bit 0, now looking at bit 1
    if(setBits % 2 == 1) //if bit1 is 1 then DOWN button (sw4) pressed
    {
      GPIOPinIntClear(GPIO_PORTE_BASE, GPIO_PIN_1);
      //make sure that scroll never becomes negative and that down scrolling only happens in status mode
      if((scroll >0 ) && (mode == 1)) 
      {
        scroll --;
      }
    }
    
    setBits = setBits >> 1; //shift out previous bit 0, now looking at bit 2
    if(setBits % 2 == 1) // if bit2 is 1 then LEFT button (sw5) pressed
    {
      GPIOPinIntClear(GPIO_PORTE_BASE, GPIO_PIN_2);
      if(mode == 1 )
        select = scroll; // if select is pressed while in status mode, then the train at the top of the list
                         // is equal to scroll, and should have it's information displayed
      
    }
    
    setBits = setBits >> 1; //shift out previous bit 0, now looking at bit 3
    if(setBits % 2 == 1) // if bit3 is 1 then RIGHT button (sw6) pressed
    {
      GPIOPinIntClear(GPIO_PORTE_BASE, GPIO_PIN_3);
      if(mode == 0) // if in annunciation mode
        mode = 1;   // go to status mode
      else
        mode = 0;   // otherwise if in status mode, go to annunciation mode
    }
    */
}
