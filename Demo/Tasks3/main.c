/*
    FreeRTOS V7.0.1 - Copyright (C) 2011 Real Time Engineers Ltd.
	

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS tutorial books are available in pdf and paperback.        *
     *    Complete, revised, and edited pdf reference manuals are also       *
     *    available.                                                         *
     *                                                                       *
     *    Purchasing FreeRTOS documentation will not only help you, by       *
     *    ensuring you get running as quickly as possible and with an        *
     *    in-depth knowledge of how to use FreeRTOS, it will also help       *
     *    the FreeRTOS project to continue with its mission of providing     *
     *    professional grade, cross platform, de facto standard solutions    *
     *    for microcontrollers - completely free of charge!                  *
     *                                                                       *
     *    >>> See http://www.FreeRTOS.org/Documentation for details. <<<     *
     *                                                                       *
     *    Thank you for using FreeRTOS, and thank you for your support!      *
     *                                                                       *
    ***************************************************************************


    This file is part of the FreeRTOS distribution and has been modified to 
    demonstrate three simple tasks running.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    >>>NOTE<<< The modification to the GPL is included to allow you to
    distribute a combined work that includes FreeRTOS without being obliged to
    provide the source code for proprietary components outside of the FreeRTOS
    kernel.  FreeRTOS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public
    License and the FreeRTOS license exception along with FreeRTOS; if not it
    can be viewed here: http://www.freertos.org/a00114.html and also obtained
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!

    http://www.FreeRTOS.org - Documentation, latest information, license and
    contact details.

    http://www.SafeRTOS.com - A version that is certified for use in safety
    critical systems.

    http://www.OpenRTOS.com - Commercial support, development, porting,
    licensing and training services.
*/


/*
 * Creates all the application tasks, then starts the scheduler.  The WEB
 * documentation provides more details of the standard demo application tasks.
 * In addition to the standard demo tasks, the following tasks and tests are
 * defined and/or created within this file:
 *
 * "OLED" task - the OLED task is a 'gatekeeper' task.  It is the only task that
 * is permitted to access the display directly.  Other tasks wishing to write a
 * message to the OLED send the message on a queue to the OLED task instead of
 * accessing the OLED themselves.  The OLED task just blocks on the queue waiting
 * for messages - waking and displaying the messages as they arrive.
 *
 * "Check" hook -  This only executes every five seconds from the tick hook.
 * Its main function is to check that all the standard demo tasks are still
 * operational.  Should any unexpected behaviour within a demo task be discovered
 * the tick hook will write an error to the OLED (via the OLED task).  If all the
 * demo tasks are executing with their expected behaviour then the check task
 * writes PASS to the OLED (again via the OLED task), as described above.
 *
 * "uIP" task -  This is the task that handles the uIP stack.  All TCP/IP
 * processing is performed in this task.
 */




/*************************************************************************
 * Please ensure to read http://www.freertos.org/portlm3sx965.html
 * which provides information on configuring and running this demo for the
 * various Luminary Micro EKs.
 *************************************************************************/

/* Set the following option to 1 to include the WEB server in the build.  By
default the WEB server is excluded to keep the compiled code size under the 32K
limit imposed by the KickStart version of the IAR compiler.  The graphics
libraries take up a lot of ROM space, hence including the graphics libraries
and the TCP/IP stack together cannot be accommodated with the 32K size limit. */

//  set this value to non 0 to include the web server

#define mainINCLUDE_WEB_SERVER		0


/* Standard includes. */
#include <stdio.h>
#include <stdlib.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* Hardware library includes. */
#include "hw_memmap.h"
#include "hw_types.h"
#include "hw_sysctl.h"
#include "driverlib/timer.h"
#include "driverlib/pwm.h"
#include "sysctl.h"
#include "gpio.h"
#include "grlib.h"
#include "rit128x96x4.h"
#include "osram128x64x4.h"
#include "formike128x128x16.h"

/* Demo app includes. */

#include "lcd_message.h"
#include "bitmap.h"

/* Project 4 includes */
#include "structs.h"
//includes for serial communication 
#include "driverlib/uart.h"
//includes for interrupts
#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"
#include "taskInfo.h"
//includes for temp measurement
#include "driverlib/adc.h"
//includes for serial output
#include "driverlib/uart.h"

/* Project 5 includes  and defines*/
#include <cmath>
#include "optfft.h"

/*-----------------------------------------------------------*/

/* 
  The time between cycles of the 'check' functionality (defined within the
  tick hook. 
*/
#define mainCHECK_DELAY	( ( portTickType ) 5000 / portTICK_RATE_MS )

// Size of the stack allocated to the uIP task.
#define mainBASIC_WEB_STACK_SIZE            ( configMINIMAL_STACK_SIZE * 3 )

// The OLED task uses the sprintf function so requires a little more stack too.
#define mainOLED_TASK_STACK_SIZE	    ( configMINIMAL_STACK_SIZE + 50 )

//  Task priorities.
#define mainQUEUE_POLL_PRIORITY		    ( tskIDLE_PRIORITY + 2 )
#define mainCHECK_TASK_PRIORITY		    ( tskIDLE_PRIORITY + 3 )
#define mainSEM_TEST_PRIORITY		    ( tskIDLE_PRIORITY + 1 )
#define mainBLOCK_Q_PRIORITY		    ( tskIDLE_PRIORITY + 2 )
#define mainCREATOR_TASK_PRIORITY           ( tskIDLE_PRIORITY + 3 )
#define mainINTEGER_TASK_PRIORITY           ( tskIDLE_PRIORITY )
#define mainGEN_QUEUE_TASK_PRIORITY	    ( tskIDLE_PRIORITY )


//The maximum number of messages that can wait for display at any one time.
  #define mainOLED_QUEUE_SIZE					( 20 )

// Dimensions the buffer into which the jitter time is written. 
  #define mainMAX_MSG_LEN						25

/* 
  The period of the system clock in nano seconds.  This is used to calculate
  the jitter time in nano seconds. 
*/

#define mainNS_PER_CLOCK ( ( unsigned portLONG ) ( ( 1.0 / ( double ) configCPU_CLOCK_HZ ) * 1000000000.0 ) )


// Constants used when writing strings to the display.

#define mainCHARACTER_HEIGHT		    ( 9 )
#define mainMAX_ROWS_128		    ( mainCHARACTER_HEIGHT * 14 )
#define mainMAX_ROWS_96			    ( mainCHARACTER_HEIGHT * 10 )
#define mainMAX_ROWS_64			    ( mainCHARACTER_HEIGHT * 7 )
#define mainFULL_SCALE			    ( 15 )
#define ulSSI_FREQUENCY			    ( 3500000UL )

/*-----------------------------------------------------------*/

/*
 * The task that handles the uIP stack.  All TCP/IP processing is performed in
 * this task.
 */
extern void vuIP_Task( void *pvParameters );

/*
 * The display is written two by more than one task so is controlled by a
 * 'gatekeeper' task.  This is the only task that is actually permitted to
 * access the display directly.  Other tasks wanting to display a message send
 * the message to the gatekeeper.
 */

static void vOLEDTask( void *pvParameters );

/*
 * Configure the hardware .
 */
static void prvSetupHardware( void );

/*
 * Configures the high frequency timers - those used to measure the timing
 * jitter while the real time kernel is executing.
 */
extern void vSetupHighFrequencyTimer( void );

/*
 * Hook functions that can get called by the kernel.
 */
void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed portCHAR *pcTaskName );
void vApplicationTickHook( void );

/*
  three dummy tasks of different priorities that simply run, announce 
  themselves, then sleep
*/

void arrivingTrainTask(void *dataStruct);
void trainComTask(void *dataStruct);
void switchControlTask(void *dataStruct);
void tempMeasurementTask(void *dataStruct);
void departingTrainTask(void *dataStruct);
void oledDisplayTask(void *dataStruct);
void localKeypadTask(void *doataStruct);
void flashingTask(void *dataStruct);
void serialComTask(void *dataStruct); //might remove
void noiseCaptureTask(void *dataStruct);
void noiseProcessTask(void *dataStruct);
void vTask3(void *vParameters);

// Functions to help with task functionality
void flash(unsigned int* flashing, DisplayStruct displayData);
void clearScreen();

//Global variables
unsigned short arrivalDirection,departureDirection,trainSize, traversalTime, 
trainArriving, arrivingWhistle,departingWhistle, mode, statusSelection, scroll,
annunciation, trainPresent, departingFlag;
short select;
unsigned short arrivingTrainDistance[8] = {0,0,0,0,0,0,0,0}; 
departingTrain departingQueue[8];
Bool checkTrain, gridlock, wheelTemp;
unsigned int gridlockDelay;
unsigned int counter, numDepartingTrains;
unsigned int temperatureBuf[8] = {0,0,0,0,0,0,0,0};
Bool flashOn;
unsigned int continueFlashing;
long setBits;
signed int timeData[256] = {0};
unsigned int freqData[16] = {0};
Bool freqDataReady; 
Bool timeDataReady;

//additional Globals
unsigned int arrivalTime;
unsigned short fiveSecondCount, fourSecondCount, threeSecondCount, 
twoSecondCount, oneSecondCount;
int aBeepStartCount;
Bool aBeepLongMode;
Bool aBeepSilenceMode;
int aBeepLONG_BLAST;
int aBeepSHORT_BLAST;
int aBeepSILENCE;
int aBeepTOTAL_LONG_BLASTS;
int aBeepTOTAL_SHORT_BLASTS;
int aBeepNumLongBlasts;
int aBeepNumShortBlasts;

int dBeepStartCount;
Bool dBeepLongMode;
Bool dBeepSilenceMode;
int dBeepLONG_BLAST;
int dBeepSHORT_BLAST;
int dBeepSILENCE;
int dBeepTOTAL_LONG_BLASTS;
int dBeepTOTAL_SHORT_BLASTS;
int dBeepNumLongBlasts;
int dBeepNumShortBlasts;
  
unsigned int arrivalTime;
Bool annuncArrival;
Bool annuncDeparture;

Bool checkBits;
Bool trainInfoDisplay;
Bool specificTrainDisplay;
Bool distanceData10Percent = FALSE;
int rapidTrainDistance[8] = {0,0,0,0,0,0,0,0};
unsigned int freqCounter;

unsigned portLONG ulY; //to write to top of display

//Global constants
unsigned const int flashingRateNorthTrain = 1;
unsigned const int flashingRateEastTrain = 20;
unsigned const int flashingRateWestTrain = 10;
unsigned const int flashingRateSouthTrain = 15;

//Task Data
ArrivingTrainStruct arrivingTrainData;
DepartingTrainStruct departingTrainData;
TrainComStruct trainComData;
DisplayStruct oledDisplayData;
SwitchControlStruct switchControlData;
LocalKeypadStruct localKeypadData;
SerialComStruct serialComData;
TempMeasStruct tempMeasData;
noiseCaptureStruct noiseCaptureData;
noiseProcessStruct noiseProcessData;


  /*char a[1]="a";
  char b[1]="b";
  char c[1]="c";
  char d[1]="d";
  char e[1]="e";
  char f[1]="f";
  char g[1]="g";*/

/*-----------------------------------------------------------*/

/* 
  The queue used to send messages to the OLED task. 
*/
xQueueHandle xOLEDQueue;

/*-----------------------------------------------------------*/

// Purpose: Initialize things that haven't been initialized already
void initializeBoard() 
{   
  
  // Set the clocking to run directly from the crystal.
  // SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN |
  //                 SYSCTL_XTAL_8MHZ);
  
  //########Initialize Hardware interrupts#########################
  
  // Enable interrupts to the processor
  IntMasterEnable();
 /* 
  //Enable the GPIO port F and E periferals
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

  //setup trainArrived ISR on sw2 
  GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_1);
  GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_1, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
  GPIOIntTypeSet(GPIO_PORTF_BASE, GPIO_PIN_1, GPIO_FALLING_EDGE);
  GPIOPinIntClear(GPIO_PORTF_BASE, GPIO_PIN_1);
  GPIOPinIntEnable(GPIO_PORTF_BASE, GPIO_PIN_1);
  IntEnable(INT_GPIOF);
  
  //setup interupts on sw3-6 for keypad presses
  //sw3 P0= scroll up
  //sw4 P1= scroll down
  //sw5 P2= mode toggle
  //sw6 P3= select train in Arrival queue
  GPIOPinTypeGPIOInput(GPIO_PORTE_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2| GPIO_PIN_3);
  GPIOPadConfigSet(GPIO_PORTE_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
  GPIOIntTypeSet(GPIO_PORTE_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_FALLING_EDGE);
  GPIOPinIntClear(GPIO_PORTE_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
  GPIOPinIntEnable(GPIO_PORTE_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
  IntEnable(INT_GPIOE);
 
  //########Initialize GPIO port for flashing
  // Enable the GPIO port that is used for the on-board LED
  //SYSCTL_RCGC2_R = SYSCTL_RCGC2_GPIOF;
  
  // Enable the GPIO pin for the LED (PF0).  Set the direction as output, and
  // enable the GPIO pin for digital function.
  // GPIO_PORTF_DIR_R = 0x01;
  // GPIO_PORTF_DEN_R = 0x01;
  
  //########Initialize PWM for Beeping##############
  unsigned long ulPeriod; 
  SysCtlPWMClockSet(SYSCTL_PWMDIV_1);
  
  // Enable the PWM and GPIO used to control it
  SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
  
  // Set GPIO C0 and G1 as PWM pins in order to output the PWM0 and
  // PWM1 signals.
  //GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_3);  this may actually work we 
  //should see what they need F3 for
  GPIOPinTypePWM(GPIO_PORTG_BASE, GPIO_PIN_1);
  GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_0);
  
  // Compute the PWM period based on the system clock.
   ulPeriod = SysCtlClockGet() / 774;

  // Set the PWM period to 440 (A) Hz.
  PWMGenConfigure(PWM_BASE, PWM_GEN_0,
                  PWM_GEN_MODE_UP_DOWN | PWM_GEN_MODE_NO_SYNC);
  PWMGenPeriodSet(PWM_BASE, PWM_GEN_0, ulPeriod);

  // Set PWM0 to a duty cycle of 25% and PWM1 to a duty cycle of 75%.
  PWMPulseWidthSet(PWM_BASE, PWM_OUT_0, ulPeriod / 4);
  PWMPulseWidthSet(PWM_BASE, PWM_OUT_1, ulPeriod * 3 / 4);

  // Enable the PWM0 and PWM1 output signals.
  PWMOutputState(PWM_BASE, PWM_OUT_0_BIT, true);
  PWMOutputState(PWM_BASE, PWM_OUT_1_BIT, false);
  
  PWMGenEnable(PWM_BASE, PWM_GEN_0);

 */ //#########Initialize the OLED display#################
  RIT128x96x4Init(1000000);
 
  //#########Initialize FFT Timer################
  
  // Enable the timer peripheral
  SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
  
  // Enable processor interrupts.
  IntMasterEnable();

  // Configure the 32-bit periodic timer for 10th of a second ticks
  TimerConfigure(TIMER1_BASE, TIMER_CFG_32_BIT_PER);
  
  // Set the timer interrupt to be above the kernel - highest. 
  IntPrioritySet( INT_TIMER1A, 0);
  
  //Load the timer
  TimerLoadSet(TIMER1_BASE, TIMER_A, SysCtlClockGet()*10);///10);

  // Setup the interrupts for the timer timeouts.
  IntEnable(INT_TIMER1A);
  TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);

  // Enable the timer.
  TimerEnable(TIMER1_BASE, TIMER_A);
  
 

  //###########initialize the UART###################################
  
  //Enable the UART and GPIO periferals
  SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
  
  // Enable Processor interrupts
  //IntMasterEnable();
  
  // Set GPIO A0 and A1 as UART pins.
  GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
  
  //Configure the UART for 115,200, 8-N-1 operation.
  UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), 115200,
                      (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                       UART_CONFIG_PAR_NONE));
  //enable UART interrupts
  IntEnable(INT_UART0);
  //Enable recieve and transmit interrupts
  UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);
  
  //##################initialize temperature sensor####################
  //SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);	// Enable Port F
  
  //ADC
  SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC);
  
  //  The trigger condition and priority is set.
  //  void ADCSequenceConfigure(ulBase, ulSequenceNum, ulTrigger, ulPriority)    
  ADCSequenceConfigure(ADC_BASE, 0, ADC_TRIGGER_TIMER, 0);
         
  //  Function will set the configuration of the ADC for one step of a sample 
  //  sequence.   
  //  void ADCSequenceStepConfigure(ulBase, ulSequenceNum, ulStep, ulConfig)
  ADCSequenceStepConfigure(ADC_BASE, 0, 0, ADC_CTL_TS | ADC_CTL_END);
  
  //  Allows  specified sample sequence to be captured when its trigger is
  //  detected.
  //  void ADCSequenceEnable(ulBase, ulSequenceNum) 
  ADCSequenceEnable(ADC_BASE, 0);
    
  //  Function enables the requested sample sequence interrupt
  //  void ADCIntEnable(ulBase, ulSequenceNum)     
  ADCIntEnable(ADC_BASE, 0);
    
  IntEnable(INT_ADC3); 
  
  //falling edge detection interrupt setup on Port D7 for frequency counter
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
  GPIOPinTypeGPIOInput(GPIO_PORTD_BASE, GPIO_PIN_7);
  GPIOPadConfigSet(GPIO_PORTD_BASE, GPIO_PIN_7, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
  GPIOIntTypeSet(GPIO_PORTD_BASE, GPIO_PIN_7, GPIO_FALLING_EDGE);
  GPIOPinIntClear(GPIO_PORTD_BASE, GPIO_PIN_7);
  GPIOPinIntEnable(GPIO_PORTD_BASE, GPIO_PIN_7);
  IntEnable(INT_GPIOD);
    
  //#########Initialize high speed timer for frequency counter################
  
  // Enable the timer peripheral
  SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);


  // Configure the 32-bit periodic timer for 10th of a second ticks
  TimerConfigure(TIMER1_BASE, TIMER_CFG_32_BIT_PER);
  TimerLoadSet(TIMER1_BASE, TIMER_B, SysCtlClockGet()/100000);
  
  //Set the timer interrupt to be above the kernel - highest. 
  IntPrioritySet( INT_TIMER1B, 0 );

  // Setup the interrupts for the timer timeouts.
  IntEnable(INT_TIMER1B);
  TimerIntEnable(TIMER1_BASE, TIMER_TIMB_TIMEOUT);

  // Enable the timer.
  TimerEnable(TIMER1_BASE, TIMER_B);
  
}

// Purpose: set the initial state of all the variables
void initializeVariables() 
{
  checkBits = FALSE;
  
  arrivalDirection = departureDirection = trainSize = -1;
  traversalTime = arrivingWhistle = -1;
  departingFlag = trainArriving = 0;
  
  departingWhistle = statusSelection = 0;
  mode = 1;   //start in Train Status mode; there should be no errors when first
              //starting program
  scroll = annunciation = trainPresent = 0;
  select = -1; //to indicate nothing has been selected
  checkTrain  = gridlock = wheelTemp = FALSE;
  counter = gridlockDelay = 0;
  flashOn = FALSE;
  continueFlashing = 0;
  setBits = 0; 
  trainInfoDisplay = TRUE;  //first display trains to select specific one
  specificTrainDisplay = FALSE;
  freqDataReady = FALSE;
  timeDataReady = FALSE;
  fiveSecondCount = 50;
  fourSecondCount = 40;
  threeSecondCount = 30;
  twoSecondCount  = 20;
  oneSecondCount  = 10;
  
  numDepartingTrains = 0;
  
  arrivalTime = 0;

  aBeepStartCount = 0;
  aBeepLongMode = 0;
  aBeepSilenceMode = 0;
  aBeepLONG_BLAST = 0;
  aBeepSHORT_BLAST = 0;
  aBeepSILENCE = 0;
  aBeepTOTAL_LONG_BLASTS = 0;
  aBeepTOTAL_SHORT_BLASTS = 0;
  aBeepNumLongBlasts = 0;
  aBeepNumShortBlasts = 0;
  
  annuncArrival = FALSE;
  annuncDeparture = FALSE;
  freqCounter = 0;
  
  //initialize departingQueue
  departingTrain train;
  train.departureDirection = -1;
  train.trainSize = 0;
  for (int i = 0; i < 10; i++)
  {
    departingQueue[i] = train; 
  }
  
  for (int i = 0; i < 8; i++)
  {
    arrivingTrainDistance[i] = 0;
    temperatureBuf[i] = 0;
  } 
}

// Purpose: initialize the data structs for the tasks
void initializeTaskData()
{

  //arriving train data initialization
  arrivingTrainData.trainArriving = &trainArriving;
  arrivingTrainData.checkTrain = &checkTrain;
  arrivingTrainData.arrivalDirection = &arrivalDirection;
  arrivingTrainData.arrivingTrainDistance = arrivingTrainDistance;
    
  //departing train data initialization
  departingTrainData.trainPresent = &trainPresent; //not sure if depricated
  departingTrainData.departureDirection = &departureDirection;
  departingTrainData.departingQueue = departingQueue;
  departingTrainData.departingFlag = &departingFlag;
  
  //temperature measurement data initialization
  tempMeasData.trainPresent = &trainPresent;
  tempMeasData.temperatureBuf = temperatureBuf;
  
  //train communication data initialization
  trainComData.trainPresent = &trainPresent;
  trainComData.checkTrain = &checkTrain;
  trainComData.trainSize = &trainSize;
  trainComData.trainArriving = &trainArriving;
  trainComData.arrivalDirection = &arrivalDirection;
  trainComData.departureDirection = &departureDirection;
  trainComData.departingFlag = &departingFlag;
  
  //switch control data initialization
  switchControlData.trainPresent = &trainPresent;
  switchControlData.trainSize = &trainSize;
  switchControlData.traversalTime = &traversalTime;
  switchControlData.gridlock = &gridlock;
  switchControlData.departingQueue = departingQueue;
  switchControlData.departingFlag = &departingFlag;
  
  //oled display data initialization
  oledDisplayData.trainPresent = &trainPresent;
  oledDisplayData.trainArriving = &trainArriving;
  oledDisplayData.trainSize = &trainSize;
  oledDisplayData.traversalTime = &traversalTime;
  oledDisplayData.departureDirection = &departureDirection;
  oledDisplayData.arrivalDirection = &arrivalDirection;
  oledDisplayData.temperatureBuf = temperatureBuf;
  oledDisplayData.arrivingTrainDistance = arrivingTrainDistance;
  oledDisplayData.departingFlag = &departingFlag;
  oledDisplayData.mode = &mode;
  oledDisplayData.statusSelection = &statusSelection;
  oledDisplayData.scroll = &scroll;
  oledDisplayData.select = &select;
  oledDisplayData.annunciation = &annunciation;
  
  //local keypad data initialization
  localKeypadData.mode = &mode;
  localKeypadData.statusSelection = &statusSelection;
  localKeypadData.scroll = &scroll;
  localKeypadData.select = &select;
  localKeypadData.annunciation = &annunciation;
  
  //serial communications data initialization
  serialComData.gridlock = &gridlock;
  serialComData.trainPresent = &trainPresent;
  serialComData.trainArriving = &trainArriving;
  serialComData.trainSize = &trainSize;
  serialComData.traversalTime = &traversalTime;
  serialComData.departureDirection = &departureDirection;
  serialComData.arrivalDirection = &arrivalDirection;
  serialComData.temperatureBuf = temperatureBuf;  
  //buffer for train distance information when difference is 10% or greater
  //between distance readings. 
  serialComData.rapidTrainDistance = rapidTrainDistance;
  /*
  arrivingTrain.i = a;
  departingTrain.i = b;
  switchControl.i = c;
  oledDisplay.i = d;
  localKeypad.i = e;
  serialCom.i = f;  
  trainCom.i = g;*/
  noiseCaptureData.timeDataReady = &timeDataReady;
  noiseCaptureData.timeData = &timeData;
  noiseProcessData.timeDataReady = &timeDataReady;
  noiseProcessData.freqDataReady = &freqDataReady;
  noiseProcessData.freqData = &freqData;
  noiseProcessData.timeData = &timeData;
}


/*************************************************************************
 * Please ensure to read http://www.freertos.org/portlm3sx965.html
 * which provides information on configuring and running this demo for the
 * various Luminary Micro EKs.
 *************************************************************************/

int main( void )
{
    prvSetupHardware();

    initializeVariables();
    initializeTaskData();
    /*  
        Create the queue used by the OLED task.  Messages for display on the OLED
        are received via this queue. 
    */
    
    xOLEDQueue = xQueueCreate( mainOLED_QUEUE_SIZE, sizeof( xOLEDMessage ) );
    

    /* 
        Exclude some tasks if using the kickstart version to ensure we stay within
        the 32K code size limit. 
    */
    
//   #if mainINCLUDE_WEB_SERVER != 0
//    {
//      /* 
//          Create the uIP task if running on a processor that includes a MAC and PHY. 
//      */
      
//      if( SysCtlPeripheralPresent( SYSCTL_PERIPH_ETH ) )
//      {
//          xTaskCreate( vuIP_Task, ( signed portCHAR * ) "uIP", mainBASIC_WEB_STACK_SIZE, NULL, mainCHECK_TASK_PRIORITY - 1, NULL );
//      }
//    }
//    #endif
    
    
    
    /* Start the tasks */
    
    xTaskCreate( vOLEDTask, ( signed portCHAR * ) "OLED", mainOLED_TASK_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL );

    xTaskCreate(arrivingTrainTask, "arrivingTrainTask", 300, &arrivingTrainData, 1, NULL);
    xTaskCreate(switchControlTask, "switchControlTask", 300, &switchControlData, 1, NULL);
    xTaskCreate(departingTrainTask, "departingTrainTask", 300, &departingTrainData, 1, NULL);
    xTaskCreate(oledDisplayTask, "oledDisplayTask", 300, &oledDisplayData, 1, NULL);
    xTaskCreate(localKeypadTask, "localKeypadTask", 300, &localKeypadData, 1, NULL);
    xTaskCreate(noiseCaptureTask, "noiseCaptureTask", 300, &noiseCaptureData, 1, NULL);
    


   // xTaskCreate(vTask2, "Task 2", 100,NULL, 2,NULL);
   // xTaskCreate(vTask3, "Task 3", 100,NULL, 3,NULL);
    
    /* 
      Configure the high frequency interrupt used to measure the interrupt
      jitter time. 
    */
    
    vSetupHighFrequencyTimer();

    /* 
      Start the scheduler. 
    */
    
    vTaskStartScheduler();

    /* Will only get here if there was insufficient memory to create the idle task. */
    
    return 0;
}

/*
  three dummy tasks
*/


void arrivingTrainTask(void *dataStruct)
{
  unsigned int fiveSecDelay = 0;
  Bool beep = FALSE;
  xTaskHandle trainComHandle, tempMeasurementHandle;
  
  ArrivingTrainStruct* arrivingTrainDataConverter = dataStruct;
  ArrivingTrainStruct arrivingTrainData = *arrivingTrainDataConverter;
  
  xOLEDMessage xMessage;
  volatile unsigned long ul;  
  const char *T1Text = "Task 1 is running\n\r";
  xMessage.pcMessage = "arrivingTrainTask";
  
  while(1)
  {
    if (fiveSecDelay == 0)
    {
      if (trainArriving != 0)
      {
        if(*arrivingTrainData.checkTrain == TRUE)
        {
          //Add trainCom task if the sensors have detected the train is within 400m of intersection
           xTaskCreate(trainComTask, "trainComTask", 100, &trainComData, 1, 
                       &trainComHandle);
        }  
        unsigned short direction = randomInteger(0, 3);
        char r[3];
        r[0] = direction + '0';
        r[1] = trainArriving + '0';
        r[2] = '\0';
         xMessage.pcMessage = r;

         xQueueSend( xOLEDQueue, &xMessage, 0 );
        *arrivingTrainData.arrivalDirection = direction;
        switch(direction)
        {
          case(1):  //north            
                  aBeepLongMode = TRUE;
                  aBeepSilenceMode = FALSE;
                  aBeepLONG_BLAST = twoSecondCount;
                  aBeepSHORT_BLAST = oneSecondCount;
                  aBeepSILENCE = oneSecondCount;
                  aBeepTOTAL_LONG_BLASTS = 2;
                  aBeepTOTAL_SHORT_BLASTS = 2;
                  aBeepNumLongBlasts = 2;
                  aBeepNumShortBlasts = 2;
                  beep = TRUE;
                  break;
         case(2):  //South
                  aBeepLongMode = TRUE;
                  aBeepSilenceMode = FALSE;
                  aBeepLONG_BLAST = twoSecondCount;
                  aBeepSHORT_BLAST = threeSecondCount;
                  aBeepSILENCE = oneSecondCount;
                  aBeepTOTAL_LONG_BLASTS = 2;
                  aBeepTOTAL_SHORT_BLASTS = 2;
                  aBeepNumLongBlasts = 2;
                  aBeepNumShortBlasts = 2;
                  beep = TRUE;
                  break;
         case(3): //East
                  aBeepLongMode = TRUE;
                  aBeepSilenceMode = FALSE;
                  aBeepLONG_BLAST = twoSecondCount;
                  aBeepSHORT_BLAST = oneSecondCount;
                  aBeepSILENCE = oneSecondCount;
                  aBeepTOTAL_LONG_BLASTS = 3;
                  aBeepTOTAL_SHORT_BLASTS = 2;
                  aBeepNumLongBlasts = 3;
                  aBeepNumShortBlasts = 2;
                  beep = TRUE;
                  break;
         case(4): //West
                  aBeepLongMode = TRUE;
                  aBeepSilenceMode = FALSE;
                  aBeepLONG_BLAST = twoSecondCount;
                  aBeepSHORT_BLAST = oneSecondCount;
                  aBeepSILENCE = oneSecondCount;
                  aBeepTOTAL_LONG_BLASTS = 1;
                  aBeepTOTAL_SHORT_BLASTS = 2;
                  aBeepNumLongBlasts = 1;
                  aBeepNumShortBlasts = 2;
                  beep = TRUE;
                  break;
        default:
                  beep = FALSE;
                  break;
        }
 
        //Add tempMeasurement task on demand
        xTaskCreate(tempMeasurementTask, "tempMeasurementTask", 100, 
                    &tempMeasData, 1, &tempMeasurementHandle);
        
        /*
        //Add trainCom task on demand
        xTaskCreate(trainComTask, "trainComTask", 100, &trainComData, 1, 
                    &trainComHandle);*/ //removed after sensor readings completed
        
        //fiveSecDelay = 50; //have to adjust for counter
        fiveSecDelay = 5;
      }
    }
    else 
    {
      //fiveSecDelay -= counter; 
      fiveSecDelay--;
    }
    
    if(beep)
    { 
      //if new seqeunce is starting then reset to start with longBlasts
      //regardless of when interrupted (may be useful in future)
      /*if(arrivingTime >= 0)
      {
        aBeepLongMode = TRUE; 
        aBeepNumLongBlasts = aBeepTOTAL_LONG_BLASTS;
        aBeepNumShortBlasts = aBeepTOTAL_SHORT_BLASTS;
      } */ 
       
      //if new beeping interval, turn on beeper and save time as aBeepStartCount
      if((aBeepStartCount == 0) && !aBeepSilenceMode)
      {
        //PWMGenEnable(PWM_BASE, PWM_GEN_0);
        PWMOutputState(PWM_BASE, PWM_OUT_1_BIT, true);
        aBeepStartCount = counter;
      }
        
      //in the future possible case that we get a train with only long or only
      //short blasts
      /*
      if(aBeepTOTAL_LONG_BLASTS == 0)
        aBeepLongMode = FALSE;
      if(aBeepTOTAL_SHORT_BLASTS ==0)
        aBeepLongMode = TRUE;*/
        
      //if in longblast mode, turn off beep after a long blast duration
      //prepare new interval of silence, and if all done with long blasts, set 
      //mode to shortblast mode
      if((((counter - aBeepStartCount) > aBeepLONG_BLAST) && aBeepLongMode) &&
         !aBeepSilenceMode)
      {
        //PWMGenDisable(PWM_BASE, PWM_GEN_0);
        PWMOutputState(PWM_BASE, PWM_OUT_1_BIT, false);
        aBeepSilenceMode = TRUE;
        aBeepStartCount = 0;
        aBeepNumLongBlasts --; //decrement number of longblasts left
        if((aBeepNumLongBlasts == 0))  //if no more longblasts left re-inint aBeepNumLongBlasts
                                    //and put in shortMode
        {
          aBeepNumLongBlasts = aBeepTOTAL_LONG_BLASTS;
          aBeepLongMode = FALSE;
        }
      }
      //if shortblast mode, then turn off beep after a short blast duration
      //prepare new interval of silence, if all done with short blasts, set mode 
      //to longblast mode
      else if((((counter - aBeepStartCount) > aBeepSHORT_BLAST) && !aBeepLongMode) && !aBeepSilenceMode)
      {
        //PWMGenDisable(PWM_BASE, PWM_GEN_0);
        PWMOutputState(PWM_BASE, PWM_OUT_1_BIT, false);
        aBeepSilenceMode = TRUE;
        aBeepStartCount = 0;
        aBeepNumShortBlasts --; //decrement number of shortblasts left 
        if((aBeepNumShortBlasts == 0)) //if no more shortblasts left re-inint 
                                       //aBeepNumShortBlasts and put in shortMode
        {
          aBeepNumShortBlasts = aBeepTOTAL_SHORT_BLASTS;
          aBeepLongMode = TRUE;
        }
      }
        
      //if new silence interval, then save the time as aBeepStartCount; 
      if((aBeepStartCount ==0) && aBeepSilenceMode)
      {
        aBeepStartCount = counter;
      }
      //check to see if (newcount - aBeepStartCount) > silence interval duration
      //if so, reset aBeepStartCount and turn off aBeepSilenceMode so that 
      //either long or short blasting can occur
      if(aBeepSilenceMode && ((counter - aBeepStartCount) > aBeepSILENCE))
      {
        aBeepStartCount = 0;
        aBeepSilenceMode = FALSE;
      }
    }
    
    //Send the message to the OLED gatekeeper for display.
    //xQueueSend( xOLEDQueue, &xMessage, 0 );

   
    vTaskDelay(1000);
  }
}

void trainComTask(void *dataStruct) 
{
  xOLEDMessage xMessage;
  volatile unsigned long ul;  
  const char *T1Text = "Task 1 is running\n\r";
  xMessage.pcMessage = "trainComTask";
  //xQueueSend( xOLEDQueue, &xMessage, 0 );
  
  TrainComStruct* trainComDataConverter = dataStruct;
  TrainComStruct trainComData = *trainComDataConverter; 
  
  unsigned int fiveSecDelay = 0;
  unsigned short outDirection;
        
  if (trainArriving != 0)
  {
    //determine outbound direction of train
    outDirection = randomInteger(0, 3);
    while (outDirection == *trainComData.arrivalDirection)
    {
      outDirection = randomInteger(0, 3);
    }
    *trainComData.departureDirection = outDirection;
      
    //determine size of train
    *trainComData.trainSize = randomInteger(1, 9);
     
    //decrement number of trains that are arriving
    trainArriving--;
      
    //set shared variables
    *trainComData.departingFlag = 1;
      
    //add train to departing queue
    departingTrain train;           //to avoid storing pointers to global vars
    train.departureDirection = outDirection;
    train.trainSize = trainSize;
    *(departingQueue + numDepartingTrains) = train;
    numDepartingTrains++;           //keep track of array index  
  }
  /*char s[2];
  s[0] = numDepartingTrains + '0';
  xMessage.pcMessage = s;
  xQueueSend( xOLEDQueue, &xMessage, 0 );*/
  //task must remove itself from the task queue
  xTaskHandle trainComHandle = xTaskGetCurrentTaskHandle();
  vTaskDelete(trainComHandle);
  *trainComData.checkTrain = FALSE;
}

void switchControlTask(void *dataStruct) 
{
  unsigned short fiveSecDelay = 0;
  short gridlockChoice;
  unsigned int traversalTimeDelay = 0;
  departingTrain train;
  
  xOLEDMessage xMessage;
  volatile unsigned long ul;  
  const char *T1Text = "Task 1 is running\n\r";
  xMessage.pcMessage = "switchControlTask";
  //xQueueSend( xOLEDQueue, &xMessage, 0 );
  
  SwitchControlStruct* switchControlDataConverter = dataStruct;
  SwitchControlStruct switchControlData = *switchControlDataConverter; 
  
  while (1)
  {
    xMessage.pcMessage = "switchControlTask";
    //xQueueSend( xOLEDQueue, &xMessage, 0 );
      
    //minor jobs
    if (gridlockDelay >= 2)
    {
      xMessage.pcMessage = "decrement gridlock";
      xQueueSend( xOLEDQueue, &xMessage, 0 );
      //gridlock so time left of delay must be decremented
      //gridlockDelay -= counter;
      gridlockDelay--;  
    }
    else if (traversalTimeDelay >= 2)
    {
      //train traversing so time left for train to traverse must be decremented
      //traversalTimeDelay -= counter;
      traversalTimeDelay--; 
    }
    
    if (fiveSecDelay == 0)
    {
      /*char r[2];
      r[0] = *switchControlData.trainPresent + '0';
      r[1] = '\0';
      xMessage.pcMessage = r;
      xQueueSend( xOLEDQueue, &xMessage, 0 );
      */
      if (*switchControlData.trainPresent != 0)
      {
        //xQueueSend( xOLEDQueue, &xMessage, 0 );
        if (traversalTimeDelay == 1)  //this turn it would be decremented to 0
                                      //train finishes traversing
        {
          //reset trainPresent
          *switchControlData.trainPresent = 0;
          traversalTimeDelay = 0;
          flashOn = FALSE;
          gridlockDelay = 0;
          continueFlashing = 0;
        }
      }
      //else if (numDepartingTrains == 0)
      //{
        //xQueueSend( xOLEDQueue, &xMessage, 0 );
        //do nothing
     // }
      else
      {
        if (*switchControlData.gridlock == FALSE && *switchControlData.traversalTime == 0)
        {
          /*gridlockChoice = randomInteger(-9,9);
          char e[2];
          e[0] = gridlockChoice + '0';
          e[1] = '\0';
          xMessage.pcMessage = e;
          xQueueSend( xOLEDQueue, &xMessage, 0 );*/
          //gridlockChoice = -7;
          gridlockChoice = 3;
          if (gridlockChoice < 0)
          {
            //gridlockDelay = (int) (0.2 * gridlockChoice * 60 * -1) + 1;
            gridlockDelay = 2;
            *switchControlData.gridlock = TRUE;
          }
          /*char w[3];
          w[0] = 'g';
          w[1] = gridlockDelay + '0';
          w[2] = '\0';
          xMessage.pcMessage = w;
          xQueueSend( xOLEDQueue, &xMessage, 0 ); */
        }
        
        if (*switchControlData.gridlock == TRUE && gridlockDelay == 1)
        {
          *switchControlData.gridlock = FALSE;
          gridlockDelay = 0;
          *switchControlData.trainPresent = 1;
          *switchControlData.departingFlag = 1;
          train = *switchControlData.departingQueue;
          
          //calculate traversal time
          //multiply by 60 to turn minutes to seconds
          unsigned int traversalTime = train.trainSize * 0.1 * 60;
          
          //calculate traversal time delay
          //multiply by 10 because 10 software delays is 1 second
          //add 1 to distinguish between when train finished traversing or when
          //train has not even started
          *switchControlData.traversalTime = traversalTime;
          //traversalTimeDelay = traversalTime * 10 + 1;
          traversalTimeDelay = 2;
        }
      }
    }
    else
    {
      //fiveSecDelay -= counter;
      fiveSecDelay--;
    }
    //Send the message to the OLED gatekeeper for display.
    //xQueueSend( xOLEDQueue, &xMessage, 0 );
    
    vTaskDelay(1000);
  }
}

void tempMeasurementTask(void *dataStruct)
{
  unsigned long dataSet[8];
  unsigned long singleReading;
  unsigned short voltage;
  unsigned char temp[9];
  
  xOLEDMessage xMessage;
  volatile unsigned long ul;  
  const char *T1Text = "Task 1 is running\n\r";
  xMessage.pcMessage = "tempMeasurementTask";
  
  TempMeasStruct* tempMeasDataConverter = dataStruct;
  TempMeasStruct tempMeasurementData = *tempMeasDataConverter;  
  
  //wait 600 milliseconds
  vTaskDelay(600);
  
  ADCSequenceDataGet(ADC_BASE, 0, dataSet);
      
  for (int i = 0; i < 8; i++)
  {
    singleReading = dataSet[i];
    voltage = (short) (1000 * 3 * 1/singleReading);  //convert to millivolts
    temp[i] = voltage + '0';
    *(tempMeasurementData.temperatureBuf + i) = voltage;
  }
  
  for (int i = 3; i < 8; i++)
  {
    if (wheelTemp == TRUE)          //if temperature too high, no need to check
                                    //the rest
    {
      break; 
    }
    for (int j = 1; j <=3; j++)
    {
      if (*(tempMeasurementData.temperatureBuf + i) > 
          (*(tempMeasurementData.temperatureBuf + (i-j)) * 1.2))
      {
        //temperature too high compared to any of previous three measurements
        wheelTemp = TRUE;
        break;
      }
    }
  }
  temp[8] = '\0';
  xMessage.pcMessage = temp;
  //xQueueSend( xOLEDQueue, &xMessage, 0 );
  
  //task must remove itself from the task queue
  xTaskHandle tempMeasHandle = xTaskGetCurrentTaskHandle();
  vTaskDelete(tempMeasHandle);
}

void departingTrainTask(void *dataStruct)
{
  xOLEDMessage xMessage;
  volatile unsigned long ul;  
  const char *T1Text = "Task 1 is running\n\r";
  xMessage.pcMessage = "departingTrainTask";
  
   
  static Bool newDeparture = TRUE;
  static Bool firstStart = TRUE;
  static Bool beep = FALSE;
  static unsigned int StartCount = 0;
  static unsigned int startDelayCount = 0; 
  static Bool noMajorJob = FALSE; //is set to false while a train is arriving
    
  unsigned int fiveSecDelay = 0;
  departingTrain train;
  Bool beepingComplete = FALSE;
  
  DepartingTrainStruct* departingTrainDataConverter = dataStruct;
  DepartingTrainStruct departingTrainData = *departingTrainDataConverter;
  
  while (1)
  {
    if (fiveSecDelay == 0)
    {
      if (*departingTrainData.departingFlag == 1)
      {
        train = *(departingTrainData.departingQueue);
        
        if (beepingComplete)  //not a minor task so have to wait until the task
                              //can do its major tasks to remove train
        {
          //Remove train at head of queue
          for (int i = 0; i < (numDepartingTrains - 1); i++)
          {
            *(departingTrainData.departingQueue + i) = *(departingTrainData.departingQueue + (i + 1));
          }
          
          //Since train was removed, decrement number of trains left
          numDepartingTrains--; 
        }
        
        //Decide beeping sequence
        switch(train.departureDirection)
        {
          case(1):  //north            
                  dBeepLongMode = TRUE;
                  dBeepSilenceMode = FALSE;
                  dBeepLONG_BLAST = twoSecondCount;
                  dBeepSHORT_BLAST = oneSecondCount;
                  dBeepSILENCE = oneSecondCount;
                  dBeepTOTAL_LONG_BLASTS = 1;
                  dBeepTOTAL_SHORT_BLASTS = 2;
                  dBeepNumLongBlasts = 1;
                  dBeepNumShortBlasts = 2;
                  beep = TRUE;
                  break;
         case(2):  //South
                  dBeepLongMode = TRUE;
                  dBeepSilenceMode = FALSE;
                  dBeepLONG_BLAST = threeSecondCount;
                  dBeepSHORT_BLAST = oneSecondCount;
                  dBeepSILENCE = oneSecondCount;
                  dBeepTOTAL_LONG_BLASTS = 1;
                  dBeepTOTAL_SHORT_BLASTS = 2;
                  dBeepNumLongBlasts = 2;
                  dBeepNumShortBlasts = 2;
                  beep = TRUE;
                  break;
         case(3): //East
                  dBeepLongMode = TRUE;
                  dBeepSilenceMode = FALSE;
                  dBeepLONG_BLAST = fourSecondCount;
                  dBeepSHORT_BLAST = oneSecondCount;
                  dBeepSILENCE = oneSecondCount;
                  dBeepTOTAL_LONG_BLASTS = 3;
                  dBeepTOTAL_SHORT_BLASTS = 2;
                  dBeepNumLongBlasts = 3;
                  dBeepNumShortBlasts = 2;
                  beep = TRUE;
                  break;
         case(4): //West
                  dBeepLongMode = TRUE;
                  dBeepSilenceMode = FALSE;
                  dBeepLONG_BLAST = fiveSecondCount;
                  dBeepSHORT_BLAST = oneSecondCount;
                  dBeepSILENCE = oneSecondCount;
                  dBeepTOTAL_LONG_BLASTS = 1;
                  dBeepTOTAL_SHORT_BLASTS = 2;
                  dBeepNumLongBlasts = 1;
                  dBeepNumShortBlasts = 2;
                  beep = TRUE;
                  break;
        default:
                  beep = FALSE;
                  break;
        }
      }
    }
    else
    {
      fiveSecDelay -= counter;
    }
    
    //Minor job
    if(beep)
    { 
      //if new beeping interval, then turn on beeper and save the time as dBeepStartCount
      if((dBeepStartCount == 0) && !dBeepSilenceMode)
      {
        //PWMGenEnable(PWM_BASE, PWM_GEN_0);
        PWMOutputState(PWM_BASE, PWM_OUT_1_BIT, true);
        dBeepStartCount = counter;
      }
      
      //if in longblast mode, turn off beep after a long blast duration
      //prepare new interval of silence, and if all done with long blasts, set 
      //mode to shortblast mode
      if((((counter - dBeepStartCount) > dBeepLONG_BLAST) && dBeepLongMode) && !dBeepSilenceMode)
      {
        //PWMGenDisable(PWM_BASE, PWM_GEN_0);
        PWMOutputState(PWM_BASE, PWM_OUT_1_BIT, false);
        dBeepSilenceMode = TRUE;
        dBeepStartCount = 0;
        dBeepNumLongBlasts --; //decrement the number of longblasts left to be performed
        if((dBeepNumLongBlasts == 0))  //if no more longblasts left set long 
                                       //blasts to off
        {
           dBeepLongMode = FALSE;
        }
      }
      //if shortblast mode, then turn off beep after a short blast duration
      //prepare new interval of silence, if all done with short blasts, set mode 
      //to longblast mode
      else if((((counter - dBeepStartCount) > dBeepSHORT_BLAST) && !dBeepLongMode) && !dBeepSilenceMode)
      {
        //PWMGenDisable(PWM_BASE, PWM_GEN_0);
        PWMOutputState(PWM_BASE, PWM_OUT_1_BIT, false);
        dBeepSilenceMode = TRUE;
        dBeepStartCount = 0;
        dBeepNumShortBlasts --; //decrement the number of shortblasts left to be performed
        if((dBeepNumShortBlasts == 0)) //if no more shortblasts left re-inint 
                                  //dBeepNumShortBlasts and put in shortMode
        {
           beepingComplete = TRUE;
        }
      }
      
      //if new silence interval, then save the time as dBeepStartCount; 
      if((dBeepStartCount ==0) && dBeepSilenceMode)
      {
          dBeepStartCount = counter;
      }
      //check to see if (newcount - dBeepStartCount) > silence interval duration
      //if so, reset dBeepStartCount and turn off dBeepSilenceMode so that either long or 
      //short blasting can occur
      if(dBeepSilenceMode && ((counter - dBeepStartCount) > dBeepSILENCE))
      {
          dBeepStartCount = 0;
          dBeepSilenceMode = FALSE;
      }
    }
    
    //xQueueSend( xOLEDQueue, &xMessage, 0 );
    vTaskDelay(1000); 
  }
}

void oledDisplayTask(void *dataStruct)
{
  xOLEDMessage xMessage;
  volatile unsigned long ul;  
  const char *T1Text = "Task 1 is running\n\r";
  xMessage.pcMessage = "oledDisplayTask";
  
  char distance[17];
  unsigned short allTrainsDisplayCount = 0;
  unsigned short specificTrainDisplayCount = 0;
  xTaskHandle flashingTaskHandle;
  
  DisplayStruct* oledDisplayDataConverter = dataStruct;
  DisplayStruct oledDisplayData = *oledDisplayDataConverter;
  
  while(1)
  {
    if (wheelTemp || (gridlockDelay != 0)) 
    {
      char p[1];
      p[0] = gridlockDelay + '0';
      p[1] = '\0';
      xMessage.pcMessage = p;
      xQueueSend( xOLEDQueue, &xMessage, 0 );
      // Error must be displayed 
      *oledDisplayData.mode = 0; //switch to Annunciation mode
      if (gridlockDelay != 0)
      {
        xMessage.pcMessage = "WARNING: Gridlock";
        //xMessage.pcMessage = p;
      }
      else if (wheelTemp)
      {
        xMessage.pcMessage = "WARNING: Wheel temp high";
      }
      clearScreen();
      xQueueSend( xOLEDQueue, &xMessage, 0 );
    }
    else if (*oledDisplayData.mode == 0)
    {
      xMessage.pcMessage = "Arriving Distance: ";
      xQueueSend( xOLEDQueue, &xMessage, 0 );
      //display arriving train distance if in Annunciation mode
      int index = 0;
      for (int i = 0; i < 16; i++)
      {
        distance[i] = *(oledDisplayData.arrivingTrainDistance + index) + '0';
        distance[++i] = ' ';
        index++;
      }
      distance[16] = '\0';
      xMessage.pcMessage = distance;
      clearScreen();
      xQueueSend( xOLEDQueue, &xMessage, 0 );
    }
    else if (*oledDisplayData.mode == 1)
    {
      if (trainInfoDisplay == TRUE) {
        if (allTrainsDisplayCount == 0)
        {
          // to only display on the screen once
          clearScreen();
          specificTrainDisplayCount = 0;
          allTrainsDisplayCount = 1;
       
          //Train Status mode and no errors
          xMessage.pcMessage = "North Train";
          xQueueSend( xOLEDQueue, &xMessage, 0 );
          
          xMessage.pcMessage = "South Train";
          xQueueSend( xOLEDQueue, &xMessage, 0 );
          
          xMessage.pcMessage = "East Train";
          xQueueSend( xOLEDQueue, &xMessage, 0 );
          
          xMessage.pcMessage = "West Train";
          xQueueSend( xOLEDQueue, &xMessage, 0 );   
          //trainInfoDisplay = FALSE;
        }
      }
      //else if (*oledDisplayData.select != -1)
      //{
        //trainInfoDisplay = FALSE;
        else if (specificTrainDisplay == TRUE)
        {
          if (specificTrainDisplayCount == 0)
          {
            // to only clear the screen once
            clearScreen();
            specificTrainDisplayCount = 1;
            allTrainsDisplayCount = 0;
          }
          //xMessage.pcMessage = "hi";
          //xQueueSend( xOLEDQueue, &xMessage, 0 );
          
          //the user has selected a train
          if (*oledDisplayData.arrivalDirection == *oledDisplayData.select)
          {
            xTaskCreate(flashingTask, "flashingTask", 200, 
                    &oledDisplayData, 1, &flashingTaskHandle);
            //Arriving train from selected direction
            /*switch (*oledDisplayData.arrivalDirection)
            {
            case 0:
              xMessage.pcMessage = "North Train";
              //xQueueSend( xOLEDQueue, &xMessage, 0 );
              flash(&flashingRateNorthTrain, oledDisplayData);
              break;
            case 1:
              xMessage.pcMessage = "South Train";
              xQueueSend( xOLEDQueue, &xMessage, 0 );
              flash(&flashingRateSouthTrain, oledDisplayData);
              break;
            case 2:
              xMessage.pcMessage = "East Train";
              xQueueSend( xOLEDQueue, &xMessage, 0 );
              flash(&flashingRateEastTrain, oledDisplayData);
              break;
            case 3:
              xMessage.pcMessage = "West Train";
              xQueueSend( xOLEDQueue, &xMessage, 0 );
              flash(&flashingRateWestTrain, oledDisplayData);
              break;
            }*/
          }
          else
          {
            //No train from selected direction arriving
            clearScreen();
            trainInfoDisplay = TRUE;
            specificTrainDisplay = FALSE;
            
          }
          //}
        //}
      }
    }
    //xQueueSend( xOLEDQueue, &xMessage, 0 );
    vTaskDelay(500); 
  }
}

// Send a string to the UART
//*****************************************************************************
void UARTSend(const unsigned char *pucBuffer, unsigned long ulCount)
{
    //
    // Loop while there are more characters to send.
    //
    while(ulCount--)
    {
        //
        // Write the next character to the UART.
        //
        UARTCharPut(UART0_BASE, *pucBuffer++);
    }
}

//This function/Task fills the timeData array with 256 samples of a sine wave
//with a frequency of 3kHz and adds the noiseProcessTask
void noiseCaptureTask(void *dataStruct)
{
  noiseCaptureStruct* noiseCaptureDataConverter = dataStruct;
  noiseCaptureStruct noiseCaptureData = *noiseCaptureDataConverter;
  
  static unsigned int sampleBuffer[256] = {0};
  static unsigned int sampleCounter = 0;
  static unsigned int index = 0;
  unsigned short maxSamples = 256;
  double pi = 31415;
  int freq = 3000;
  unsigned int magnitudeMax = 3;
  xTaskHandle noiseProcHandle; 

  while(1)
  {
     sampleCounter ++;
     vTaskDelay(3); //wait .0003 milliseconds for 3.75KHz
     if((index < maxSamples) && (*noiseCaptureData.timeDataReady != TRUE))
     {
       // 3sin((2pi(freq))(elapsed secs)
       sampleBuffer[index]= magnitudeMax*sin(2*(pi/10000)*freq*(3*sampleCounter/1000)); 
       index++;
     }
     else
     {
       for(int i=0; i < maxSamples; i++)
          noiseCaptureData.timeData[i] = sampleBuffer[i]; //transfer local temp buffer
                                                          //over to global buffer when data is ready. 
       *noiseCaptureData.timeDataReady = TRUE; //signal that timeData is ready to be consumed.
       index = 0; //reset timeData fill position to index 0.
       xTaskCreate(noiseProcessTask, "noiseProcessTask", 100, &noiseProcessData, 1, 
                       &noiseProcHandle); //add a noiseProcessTask to schedule
     } 
  }

}

//this function/Task fills the freqData array with 16 frequency
//values correspoinding to the FFT of the timeSampled frequency
//signals the webserver that the frequency values are ready
//and deletes itself. 
void noiseProcessTask(void *dataStruct)
{
  
  noiseProcessStruct* noiseProcessDataConverter = dataStruct;
  noiseProcessStruct noiseProcessData = *noiseProcessDataConverter;
  int maxFreqs = 16;
  static int index = 0;
  signed int allZeros[256] = {0};
  xTaskHandle noiseProcHandle = xTaskGetCurrentTaskHandle();
  *noiseProcessData.freqDataReady = FALSE; //as soon as the task is scheduled
                                           //no freq data is valid
  
  while(1)
  {
     if(*noiseProcessData.timeDataReady)
     {
        //determine signal frequency 
        //optfft function is by Brent Plump, see citation in optfft.c 
        noiseProcessData.freqData[index] = optfft(noiseProcessData.timeData,allZeros); 
        //signal time data that timeData has been consumed
        *noiseProcessData.timeDataReady = FALSE;    
        index++;
        if(index >= 15)
        {
          //signal webserver that freqData is ready to be consumed
          *noiseProcessData.freqDataReady = TRUE;
          index = 0;
          vTaskDelete(noiseProcHandle); //task deletes itself after filling out 16 
                                        //valid frequency values and signalling
          
        }
     }
     vTaskDelay(1000);
  }
}

void serialComTask(void *dataStruct)
{
  
  while(1)
  {
    
    //UARTSend((unsigned char *)"Send test: ", 11);
    SerialComStruct* serialComDataConverter = dataStruct;
    SerialComStruct serialComData = *serialComDataConverter;
    
    xOLEDMessage xMessage;
    volatile unsigned long ul;  
    //const char *T1Text = "Task 1 is running\n\r";
    xMessage.pcMessage = "serialComTask";
    clearScreen();
    xQueueSend( xOLEDQueue, &xMessage, 0 );
 
    static unsigned int twoSecCounter = 20;
    static Bool firstPass = TRUE;
    char charArray[690] = ""; //this doesn't work because the stack can't handle 300*
  
    if((twoSecCounter < 1 ) || firstPass)
    {
      twoSecCounter = 5;
      firstPass = false;
      char *buffer;
      int memsize = 690;
      buffer = charArray;
      
      char dDirection[5] = "";
      char Direction[5] = "";
      char intAvailable[3] = "";
      char glOn[3] = "";
      int n;
      char* point = NULL;
      char nor[] = "North";
      char e[] = "East";
      char s[] = "South";
      char w[] = "West";
      char o[] = "Off";
      char yes[] = "Yes";
      char no[] = "No";
      switch(*serialComData.departureDirection)
      {
        case 0: 
          n = sprintf(dDirection,"%s",o);
        break;
        case 1: 
        n = sprintf(dDirection, "%s",nor);
        break;
        case 2:
        n = sprintf(dDirection,"%s",s);
        break;
        case 3: 
        n = sprintf(dDirection,"%s",e);
        break;
        case 4: 
        n = sprintf(dDirection,"%s",w);
        break;
        default: 
        n = sprintf(dDirection,"%s",o);
      }
     
       switch(*serialComData.departureDirection)
      {
        case 0: 
          n = sprintf(Direction,"%s",o);
        break;
        case 1: 
        n = sprintf(Direction, "%s",nor);
        break;
        case 2:
        n = sprintf(Direction,"%s",s);
        break;
        case 3: 
        n = sprintf(Direction,"%s",e);
        break;
        case 4: 
        n = sprintf(Direction,"%s",w);
        break;
        default: 
        n = sprintf(Direction,"%s",o);
      }
      
      if(*serialComData.traversalTime < 1)
         n = sprintf(intAvailable,"%s",yes);
      else 
         n = sprintf(intAvailable,"%s",no);
      
      if(*serialComData.gridlock == TRUE)
         n = sprintf(glOn,"%s", yes);
      else 
         n = sprintf(glOn,"%s",no);
      
      int l = sprintf(buffer, 
                      "\fTrain Present%20d\r\nCurrent Direction%20s\r\nTrain Size%20d\r\nTraversal Time%20d\r\nIntersection Availability%20s\r\nTrain Arriving%20s\r\nArrival Time%20d\r\nGridlock%20s\r\nArriving Train Distance1%20d\r\nArriving Train Distance2%20d\r\nArriving Train Distance3%20d\r\nArriving Train Distance4%20d\r\nArriving Train Distance5%20d\r\nArriving Train Distance6%20d\r\nArriving Train Distance7%20d\r\nArriving Train Distance8%20d",
                      40, trainArriving, dDirection,trainSize,traversalTime,intAvailable,Direction,arrivalTime,glOn,rapidTrainDistance[0],rapidTrainDistance[1],rapidTrainDistance[2],rapidTrainDistance[3],rapidTrainDistance[4],rapidTrainDistance[5],rapidTrainDistance[6],rapidTrainDistance[7]);
      
      //int l = sprintf(buffer, "i there");
      UARTSend((unsigned char*)buffer,memsize);
      //free(buffer); //return the memory used to store status character strings
   }
   twoSecCounter --; 
      
   vTaskDelay(1000);
  }

}

void localKeypadTask(void *dataStruct)
{
  xOLEDMessage xMessage;
  volatile unsigned long ul;  
  const char *T1Text = "Task 1 is running\n\r";
  xMessage.pcMessage = "localKeypadTask";
  
  LocalKeypadStruct* keypadDataConverter = dataStruct;
  LocalKeypadStruct keypadData = *keypadDataConverter;
  
  while(1)
  {
    if(setBits % 2 == 1) //if leftmost bit (bit0) is 1 then UP button (sw3) pressed
    {
      //make sure scroll does not exceed max trainsList size and that up scrolling only happens in status mode
      if((*keypadData.scroll > 0 ) && (*keypadData.mode == 1))
          (*keypadData.scroll)--;
          //trainArriving--; //TODO: remove debug
          //list all trains at or above scroll
    }
    
    setBits = setBits >> 1; //shift out previous bit 0, now looking at bit 1
    if(setBits % 2 == 1) //if bit1 is 1 then DOWN button (sw4) pressed
    {
      //make sure that scroll never becomes negative and that down scrolling only happens in status mode
      if(*keypadData.mode == 1)
      {
        (*keypadData.scroll)++;
      }
    }
    
    setBits = setBits >> 1; //shift out previous bit 0, now looking at bit 2
    if(setBits % 2 == 1) // if bit2 is 1 then LEFT button (sw5) pressed
    {
      if(*keypadData.mode == 1 )
      {
        if (trainInfoDisplay == TRUE)
        {
          *keypadData.select = *keypadData.scroll; // if select is pressed while in status mode, then the train at the top of the list
                                                   // is equal to scroll, and should have it's information displayed
          trainInfoDisplay = FALSE;
          specificTrainDisplay = TRUE;
          //clearScreen();
        }
        else
        {
          trainInfoDisplay = TRUE;
          specificTrainDisplay = FALSE;
          //clearScreen();
        }
      }      
    }
    
    setBits = setBits >> 1; //shift out previous bit 0, now looking at bit 3
    if(setBits % 2 == 1) // if bit3 is 1 then RIGHT button (sw6) pressed
    {
      if(*keypadData.mode == 0) { // if in annunciation mode
        *keypadData.mode = 1;   // go to status mode
        *keypadData.select = -1;
        trainInfoDisplay = TRUE;
        specificTrainDisplay = FALSE;
      }
      else
        *keypadData.mode = 0;   // otherwise if in status mode, go to annunciation mode
    }
    
    
    //if (*keypadData.select == -1)
    //{
    char r[2];
    r[0] = mode + '0';
    r[1] = '\0';
    xMessage.pcMessage = r;
    //xQueueSend( xOLEDQueue, &xMessage, 0 );
    //}
    vTaskDelay(2000); 
  }
}

void flashingTask(void* dataStruct)
{
  xOLEDMessage xMessage;
  
  DisplayStruct* oledDisplayDataConverter = dataStruct;
  DisplayStruct displayData = *oledDisplayDataConverter;
  
  if (flashOn == FALSE && continueFlashing == 0) //turn light on
  {  
    //display train information
    if (*displayData.trainPresent == 0)
    {
      xMessage.pcMessage = "Train Present";
      xQueueSend( xOLEDQueue, &xMessage, 0 );
      switch (*displayData.arrivalDirection)
      {
        case 0:
          xMessage.pcMessage = "Arriving from North";
          continueFlashing = flashingRateNorthTrain;
          break;
        case 1:
          xMessage.pcMessage = "Arriving from South";
          continueFlashing = flashingRateSouthTrain;
          break;
        case 2:
          xMessage.pcMessage = "Arriving from East";
          continueFlashing = flashingRateEastTrain;
          break;
        case 3:
          xMessage.pcMessage = "Arriving from West";
          continueFlashing = flashingRateWestTrain;
          break;
      }
      xQueueSend( xOLEDQueue, &xMessage, 0 );
    }
    else if (*displayData.trainArriving != 0)
    {
      xMessage.pcMessage = "Train Arriving";
      xQueueSend( xOLEDQueue, &xMessage, 0 );
      switch (*displayData.departureDirection)
      {
        case 0:
          xMessage.pcMessage = "Going North";
          break;
        case 1:
          xMessage.pcMessage = "Going South";
          break;
        case 2:
          xMessage.pcMessage = "Going East";
          break;
        case 3:
          xMessage.pcMessage = "Going West";
          break;
      }
      xQueueSend( xOLEDQueue, &xMessage, 0 );
    }
    
    char* w;
    *(w+0) = 'h';
    *(w+1) = '\0';
     xMessage.pcMessage = w;
    xQueueSend( xOLEDQueue, &xMessage, 0 );
    
    char distance[17];
    //display arriving train distance 
    int index = 0;
    for (int i = 0; i < 16; i++)
    {
      distance[i] = *(oledDisplayData.arrivingTrainDistance + index) + '0';
      distance[++i] = ' ';
      index++;
    }
    distance[16] = '\0';
    xMessage.pcMessage = distance;
    xQueueSend( xOLEDQueue, &xMessage, 0 );
    
    xMessage.pcMessage = "hello";
    xQueueSend( xOLEDQueue, &xMessage, 0); 
    
    char trainSize[2];
    trainSize[0] = *displayData.trainSize + '0';
    trainSize[1] = '\0';
    xMessage.pcMessage = trainSize;
    xQueueSend( xOLEDQueue, &xMessage, 0 );
    
    char traversalTime[2];
    trainSize[0] = *displayData.traversalTime + '0';
    trainSize[1] = '\0';
    xMessage.pcMessage = traversalTime;
    xQueueSend( xOLEDQueue, &xMessage, 0 );
      
    //continueFlashing = *flashing;     
    flashOn = TRUE;
  }
  else if (flashOn == TRUE && continueFlashing == 0) //turn light off
  {
    clearScreen();
    //continueFlashing = *flashing;     
    switch (*displayData.arrivalDirection)
      {
        case 0:
          continueFlashing = flashingRateNorthTrain;
          break;
        case 1:
          continueFlashing = flashingRateSouthTrain;
          break;
        case 2:
          continueFlashing = flashingRateEastTrain;
          break;
        case 3:
          continueFlashing = flashingRateWestTrain;
          break;
      }
    flashOn = FALSE;
  }
  else              
  {
    //decrement time left for the particular cycle of flashing
    continueFlashing--;
  }
  xTaskHandle tempMeasHandle = xTaskGetCurrentTaskHandle();
  vTaskDelete(tempMeasHandle);
}

void clearScreen()
{
  RIT128x96x4Clear();
  ulY = 0;
}


/*
  the OLED Task
*/

void vOLEDTask( void *pvParameters )
{
    xOLEDMessage xMessage;
    unsigned portLONG ulMaxY;
    static portCHAR cMessage[ mainMAX_MSG_LEN ];
    extern volatile unsigned portLONG ulMaxJitter;
    unsigned portBASE_TYPE uxUnusedStackOnEntry;
    const unsigned portCHAR *pucImage;

// Functions to access the OLED. 

    void ( *vOLEDInit )( unsigned portLONG ) = NULL;
    void ( *vOLEDStringDraw )( const portCHAR *, unsigned portLONG, unsigned portLONG, unsigned portCHAR ) = NULL;
    void ( *vOLEDImageDraw )( const unsigned portCHAR *, unsigned portLONG, unsigned portLONG, unsigned portLONG, unsigned portLONG ) = NULL;
    void ( *vOLEDClear )( void ) = NULL;
  
  
    vOLEDInit = RIT128x96x4Init;
    vOLEDStringDraw = RIT128x96x4StringDraw;
    vOLEDImageDraw = RIT128x96x4ImageDraw;
    vOLEDClear = RIT128x96x4Clear;
    ulMaxY = mainMAX_ROWS_96;
    pucImage = pucBasicBitmap;
              
// Just for demo purposes.
    uxUnusedStackOnEntry = uxTaskGetStackHighWaterMark( NULL );
  
    ulY = ulMaxY;
    
    /* Initialise the OLED  */
    vOLEDInit( ulSSI_FREQUENCY );	
    
    while( 1 )
    {
      // Wait for a message to arrive that requires displaying.
      
      xQueueReceive( xOLEDQueue, &xMessage, portMAX_DELAY );
  
      // Write the message on the next available row. 
      
      ulY += mainCHARACTER_HEIGHT;
      if( ulY >= ulMaxY )
      {
          ulY = mainCHARACTER_HEIGHT;
          vOLEDClear();
      }
  
      // Display the message  
                      
      sprintf( cMessage, "%s", xMessage.pcMessage);
      
      vOLEDStringDraw( cMessage, 0, ulY, mainFULL_SCALE );
      
  }
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed portCHAR *pcTaskName )
{
    ( void ) pxTask;
    ( void ) pcTaskName;
  
    while( 1 );
}

/*-----------------------------------------------------------*/

void prvSetupHardware( void )
{
    /* 
      If running on Rev A2 silicon, turn the LDO voltage up to 2.75V.  This is
      a workaround to allow the PLL to operate reliably. 
    */
  
    if( DEVICE_IS_REVA2 )
    {
        SysCtlLDOSet( SYSCTL_LDO_2_75V );
    }
	
    // Set the clocking to run from the PLL at 50 MHz
    
    SysCtlClockSet( SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_8MHZ );
    
    /* 	
      Enable Port F for Ethernet LEDs
            LED0        Bit 3   Output
            LED1        Bit 2   Output 
    */
    
    SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOF );
    GPIODirModeSet( GPIO_PORTF_BASE, (GPIO_PIN_2 | GPIO_PIN_3), GPIO_DIR_MODE_HW ); //Why?
    GPIOPadConfigSet( GPIO_PORTF_BASE, (GPIO_PIN_2 | GPIO_PIN_3 ), GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD ); //Again why?	
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /*Now call Proj4 specific board initializations that don't conflict*/
    initializeBoard();
	
}


/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{
    static xOLEDMessage xMessage = { "PASS" };
    static unsigned portLONG ulTicksSinceLastDisplay = 0;
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

    /* 
      Called from every tick interrupt.  Have enough ticks passed to make it
      time to perform our health status check again? 
    */
    
    ulTicksSinceLastDisplay++;
    if( ulTicksSinceLastDisplay >= mainCHECK_DELAY )
    {
       ulTicksSinceLastDisplay = 0;
            
    }
}

//converts a 100Hz frequency to 2km and 900Hz frequency to 100m
//sets checkTrain to true if distance is less than < 400m
volatile void freqToDistance(void)
{
  GPIOPinIntClear(GPIO_PORTD_BASE, GPIO_PIN_7);
  static int trainDistanceBuff[8] = {0,0,0,0,0,0,0,0}; 
  static unsigned int index = 0;
  static unsigned int firstNum = 0;
  static unsigned int flipCounter = 1;
  char chars[5] = "";
  const char* charptr = chars;
  const int countsPerSec = 100000;
  const int yIntercept = 94200;
  const int slope = -42;
  unsigned int correctionFactor = 65; //correction factor that works from 100-990Hz interrupts miss 65 counts consistently
                                      //not sure why falling edge detection performance is frequency dependent. 
  int pulseFreq = 0;
  xTaskHandle trainComHandle;
  
  xOLEDMessage xMessage;
  int MAX_LEN = 7;
  int MAX_DIFFERENCE = 10;
    
  if(flipCounter >= 2)
  {
    pulseFreq = countsPerSec/((freqCounter - firstNum + correctionFactor)); //frequency of pulses
    trainDistanceBuff[index]= ((pulseFreq*100 - yIntercept)/ slope);
    sprintf(chars,"%d",trainDistanceBuff[index]);
    if((trainDistanceBuff[index] <= 400) && (checkTrain == FALSE))
    {
      checkTrain = TRUE;
    }  
    index++;
    if(index > MAX_LEN)
    {
      index = 0;
      //copy the computed distances over to the global arrivingTrainDistance 
      for(int j=0; j <8; j++)
        arrivingTrainDistance[j] = trainDistanceBuff[j];
    } 
    if(((flipCounter - firstNum)/((flipCounter + firstNum)/2.0))*100 > MAX_DIFFERENCE) //if any reading differs by 10%
    {
      distanceData10Percent = TRUE;
      for(int j=0; j <8; j++)
        rapidTrainDistance[j] = trainDistanceBuff[j];
      
      //Add trainCom task if the sensors have detected the train is within 400m of intersection
      xTaskCreate(trainComTask, "trainComTask", 100, &trainComData, 1, 
                       &trainComHandle);
    }
   
     
  }   
  
  firstNum = freqCounter;
  flipCounter++;
}